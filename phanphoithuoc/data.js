var data = [
 {
   "STT": 1,
   "Name": "Bệnh viện Đa khoa Tỉnh THÁI BÌNH",
   "area": "Tỉnh",
   "Number_of_beds": 600,
   "address": "60 Ung Văn Khiêm, P  Mỹ Phước, Thành phố Long Xuyên , Tỉnh THÁI BÌNH",
   "Longtitude": 10.36974,
   "Latitude": 105.435874
 },
 {
   "STT": 2,
   "Name": "Bệnh viện Mắt - Tai Mũi Họng - Răng Hàm Mặt THÁI BÌNH",
   "area": "Tỉnh",
   "Number_of_beds": 73,
   "address": "12 Lê Lợi, P  Mỹ Bình, Thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.387332,
   "Latitude": 105.439444
 },
 {
   "STT": 3,
   "Name": "Bệnh viện Tim Mạch THÁI BÌNH",
   "area": "Tỉnh",
   "Number_of_beds": 300,
   "address": "08 Nguyễn Du, P  Mỹ Bình, Thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.389386,
   "Latitude": 105.438659
 },
 {
   "STT": 4,
   "Name": "Bệnh viện sản nhi THÁI BÌNH",
   "area": "Tỉnh",
   "Number_of_beds": 300,
   "address": "số 02 Lê Lợi, phường Mỹ Bình, TP Long Xuyên",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 5,
   "Name": "Trung tâm y tế Châu Đốc",
   "area": "Huyện",
   "Number_of_beds": null,
   "address": "Khu dân cư Khóm 8, Phường Châu Phú A, Thành phố  Châu Đốc, Tỉnh THÁI BÌNH",
   "Longtitude": 10.699519,
   "Latitude": 105.103478
 },
 {
   "STT": 6,
   "Name": "Bệnh viện Đa khoa khu vực Tân Châu",
   "area": "Tỉnh",
   "Number_of_beds": 200,
   "address": "Nguyễn Tri Phương, Thị trấn  Tân Châu, Tân Châu, THÁI BÌNH",
   "Longtitude": 10.789003,
   "Latitude": 105.231925
 },
 {
   "STT": 7,
   "Name": "Trung tâm y tế huyện Thoại Sơn",
   "area": "Huyện",
   "Number_of_beds": null,
   "address": "Thoại Giang, Thoại Sơn, THÁI BÌNH",
   "Longtitude": 10.259472,
   "Latitude": 105.258637
 },
 {
   "STT": 8,
   "Name": "Trung tâm y tế huyện An Phú",
   "area": "Huyện",
   "Number_of_beds": null,
   "address": "Đường Bờ Bắc Kinh Mới, Thị Trấn An Phú, Huyện An Phú, Tỉnh THÁI BÌNH",
   "Longtitude": 10.816253,
   "Latitude": 105.08698
 },
 {
   "STT": 9,
   "Name": "Trung tâm y tế huyện Châu Thành",
   "area": "Huyện",
   "Number_of_beds": null,
   "address": "ấp Hoà Phú 3, Thị trấn An Châu,, Huyện Châu Thành, THÁI BÌNH",
   "Longtitude": 10.440109,
   "Latitude": 105.393211
 },
 {
   "STT": 10,
   "Name": "Trung tâm y tế huyện Châu Phú",
   "area": "Huyện",
   "Number_of_beds": null,
   "address": "QL91, Ấp Vĩnh Phú, Xã Vĩnh Thạnh Trung, Huyện Châu Phú, Tỉnh THÁI BÌNH",
   "Longtitude": 10.597605,
   "Latitude": 105.222175
 },
 {
   "STT": 11,
   "Name": "Trung tâm y tế huyện Chợ Mới",
   "area": "Huyện",
   "Number_of_beds": null,
   "address": "ấp thị II, Trấn Chợ Mới, Huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.544542,
   "Latitude": 105.400399
 },
 {
   "STT": 12,
   "Name": "Trung tâm y tế huyện Phú Tân",
   "area": "Huyện",
   "Number_of_beds": null,
   "address": "Số 123 Hải Thượng Lãn Ông, Thị trấn Phú Mỹ, Huyện Phú Tân, Tỉnh THÁI BÌNH",
   "Longtitude": 10.597595,
   "Latitude": 105.351322
 },
 {
   "STT": 13,
   "Name": "Trung tâm y tế huyện Tinh Biên",
   "area": "Huyện",
   "Number_of_beds": null,
   "address": "Đường Hải Thượng Lãn Ông, Thị trấn  Nha Bàng, THÁI BÌNH",
   "Longtitude": 10.623378,
   "Latitude": 105.014961
 },
 {
   "STT": 14,
   "Name": "Trung tâm y tế huyện Tri Tôn",
   "area": "Huyện",
   "Number_of_beds": null,
   "address": "72, Nguyễn Trãi, Thị trấn Tri Tôn, Huyện Tri Tôn,Tỉnh THÁI BÌNH",
   "Longtitude": 10.421675,
   "Latitude": 104.998028
 },
 {
   "STT": 15,
   "Name": "Bệnh viện Đa khoa Khu Vực Châu Đốc",
   "area": "Tỉnh",
   "Number_of_beds": 500,
   "address": "Phường Vĩnh Mỹ, Châu Đốc, THÁI BÌNH",
   "Longtitude": 10.689438,
   "Latitude": 105.148312
 },
 {
   "STT": 16,
   "Name": "Trung tâm y tế thị xã Tân Châu",
   "area": "Huyện",
   "Number_of_beds": null,
   "address": "Tỉnh lộ 953, Phường Long Thạnh, Thị xã Tân Châu, Tỉnh THÁI BÌNH  ",
   "Longtitude": 10.789025,
   "Latitude": 105.231915
 },
 {
   "STT": 17,
   "Name": "Bệnh viện Đa khoa tư nhân Bình Dân THÁI BÌNH",
   "area": "",
   "Number_of_beds": null,
   "address": "39 Trần Hưng Đạo, Mỹ Long, Thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.37517,
   "Latitude": 105.441592
 },
 {
   "STT": 18,
   "Name": "Bệnh viện Đa khoa Hạnh Phúc THÁI BÌNH",
   "area": "",
   "Number_of_beds": null,
   "address": "234 Trần Hưng Đạo, P  Mỹ Thới, Thành phố  Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.355362,
   "Latitude": 105.458245
 },
 {
   "STT": 19,
   "Name": "Trung tâm y tế Long Xuyên",
   "area": "Huyện",
   "Number_of_beds": null,
   "address": "9 Hải Thượng Lãn Ông, Mỹ Xuyên, Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.37456,
   "Latitude": 105.440839
 }
];