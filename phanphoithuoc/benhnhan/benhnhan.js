var benhthuoc = [
 ['Viêm họng','Penicillin, Amoxilin'],
 ['Đường tiêu hóa','Oresol, Hydrite'],
 ['Ho','Theralene'],
 ['Alzheimers','Namenda'],
 ['Bệnh phổi tắc nghẽn mạn tính','Spiriva, Daliresp'],
 ['Nghiện thuốc lá','Chantix'],
 ['Co giật','Lyrica, Dilantin'],
 ['Chẹn kênh canxi','Procardia, Verelan'],
 ['Động kinh','Neurontin, Topamax'],
 ['Kết tập tiểu cầu','Brilinta, Plavix'],
 ['Loạn thần','Seroquel, Zyprexa'],
 ['Rối loạn tiểu tiện','Detrol, Cardura'],
 ['Trầm cảm','Ativan, Pristiq'],
 ['Đái tháo đường','Glucophage'],
];

var nhathuoc = [
['Nhà thuốc Long Châu', 'Anginovag 10ml', 'Lọ', '106000', '700',  '10/10/2018'],
['Nhà thuốc Long Châu', 'Kary Uni Ophthalmic Suspension 0,05mg/ml', 'Lọ', '22000', '1524',  '10/10/2018'],
['Nhà thuốc Long Châu', 'Systane Ultra 5ml', 'Lọ', '61000', '2410',  '10/10/2018'],
['Nhà thuốc Long Châu', 'Zinnat Sus Sac 125mg/5 ml 10', 'Gói', '15000', '200',  '10/10/2018'],
['Nhà thuốc Long Châu', 'Medrol Tab', 'Viên', '1000', '10200',  '10/10/2018'],
['Nhà thuốc 365', 'Dexamethasone', 'Ống', '882', '6000',  '20/10/2018'],
['Nhà thuốc 365', 'Grammar', 'Chai', '234000', '600',  '20/10/2018'],
['Nhà thuốc 365', 'Vitamin C 500mg', 'Viên', '137', '700',  '20/10/2018'],
['Nhà thuốc Phương Chính', 'Anginovag 10ml', 'Lọ', '106000', '700',  '1/10/2018'],
['Nhà thuốc Phương Chính', 'Kary Uni Ophthalmic Suspension 0,05mg/ml', 'Lọ', '22000', '1524',  '1/10/2018'],
['Nhà thuốc Phương Chính', 'Systane Ultra 5ml', 'Lọ', '61000', '2410',  '1/10/2018'],
['Siêu thị thuốc Mega3', 'Zinnat Sus Sac 125mg/5 ml 10', 'Gói', '15000', '200',  '18/10/2018'],
['Siêu thị thuốc Mega3', 'Medrol Tab', 'Viên', '1000', '10200',  '18/10/2018'],
['Nhà thuốc Thanh Mai', 'Dexamethasone', 'Ống', '882', '6000',  '27/10/2018'],
['Nhà thuốc Thanh Mai', 'Grammar', 'Chai', '234000', '600',  '27/10/2018'],
['Nhà thuốc Thanh Mai', 'Vitamin C 500mg', 'Viên', '137', '700',  '27/10/2018'],
];

var tuongtacthuoc = [
['1', 'Amikacin - Furosemid', 'Do tác dụng dược lí hiệp đồng.Tăng nguy cơ độc với tai và thận', '', 'Tránh dùng chung nếu được'],
['1', 'Amikacin - Furosemid', 'Do tác dụng dược lí hiệp đồng.Tăng nguy cơ độc với tai và thận', '', 'Nếu dùng chung là bắt buộc cần kiểm tra chức năng thận ,tiền đình,thính lực là cần thiết nếu bắt buộc điều trị chung'],
]
var thuocmoi = [
{tenthuoc: 'Lidocain', nongdo: '2%/2ml', dangbaoche: 'Dung dịch tiêm', donggoi: 'Hộp 20 ống', nhasx: 'Hải Dương', sdk: 'VD-11228-10', donvitinh: 'ống', dongia: '480', nhacungcap: 'Hải Dương'},
{tenthuoc: 'Povidon iodin Antiseptic', nongdo: '10% -90ml', dangbaoche: 'Dung dịch dùng ngoài', donggoi: 'Chai 90ml', nhasx: 'Hóa Dược', sdk: 'VD-1736-12', donvitinh: 'Chai', dongia: '12400', nhacungcap: 'Hải Dương'},
{tenthuoc: 'Ringer Lactate 500ml', nongdo: '500ml', dangbaoche: 'Dung dịch tiêm truyền , Tiêm truyền tĩnh mạch', donggoi: 'Chai nhựa 500ml', nhasx: 'Fresenius Kabi Bidiphar',
sdk: 'VD-22591-15', donvitinh: 'Chai nhựa PE', dongia: '6615', nhacungcap: 'Công ty cổ phần dược phẩm  Thiết bị y tế THÁI BÌNH'},
{tenthuoc: 'Tetracycllin 1% 5g', nongdo: '1%/5g', dangbaoche: 'Mỡ tra mắt', donggoi: 'Hộp 1 tuýp', nhasx: 'Medipharco - Tenamyd',
sdk: 'VD-12463-10', donvitinh: 'Tuýp', dongia: '2415', nhacungcap: 'Medipharco - Tenamyd'},
{tenthuoc: 'Dexamethason', nongdo: '4mg/ml', dangbaoche: 'Dung dịch tiêm ',
donggoi: 'Hộp 10 ống', nhasx: 'Quảng Bình',
sdk: 'VD-10357-10', donvitinh: 'Tuýp', dongia: '2599', nhacungcap: ' CPC1'},
] 
$(document).ready(function() {
  thuocmoi.forEach(function(item, index) {
    $("#thuoc-moi-cap-nhat").append(
     '<tr>\
     <td>'+(index + 1)+'</td>\
     <td>'+item.tenthuoc+'</td>\
     <td>'+item.nongdo+'</td>\
     <td>'+item.dangbaoche+'</td>\
     <td>'+item.donggoi+'</td>\
     <td>'+item.nhasx+'</td>\
     <td>'+item.sdk+'</td>\
     <td>'+item.donvitinh+'</td>\
     <td>'+item.dongia+'</td>\
     <td>'+item.nhacungcap+'</td>\
     </tr>'
     )
  })
  
 // ----------- TƯƠNG TÁC THUỐC--------------
 // $("#input-tuong-tac").on("keyup", function() {
 //    var value = $(this).val().toLowerCase();
 //    $("#table-tuongtacthuoc tr").filter(function() {
 //      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
 //    });
 //  });
 // ----------- TƯƠNG TÁC THUỐC--------------



// ----------- DANH MỤC THUỐC -----------
  $('#table-nhathuoc').DataTable({
    columns: [
      {title: 'Nhà thuốc',},
      {title: 'Tên thuốc',}, 
      {title: 'Cách đóng gói',},
      {title: 'Giá bán (VNĐ)',},
      {title: 'Số lượng bán',},
      {title: 'Ngày cập nhật',}
    ],
    data: nhathuoc,
    spans:true,
    rowsGroup: [0, 5],
    pageLength: '10',
  });
// ----------- DANH MỤC THUỐC -----------

// ------------ TÌM BỆNH VÀ THUỐC TƯƠNG ỨNG-----------
$('#table-benh').DataTable({
    columns: [
      {title: 'Bệnh',},
      {title: 'Thuốc',}, 
    ],
    data: benhthuoc,
    spans:true,
    rows: true,
    pageLength: '10',
  });
// ------------ TÌM BỆNH VÀ THUỐC TƯƠNG ỨNG----------

// ----------- KIỂM TRA MÃ VẠCH---------
  $('.btn-ma-vach').click(function(){
    var mavach = $('#input-ma').val();
    var flag = false;
    var tong = 0;
    if(mavach == ""){
      
     flag = false;
   } else if(mavach.length != 13 && mavach.length > 0){
     flag = false;
   }
   else{
    for(i=0; i<mavach.length - 1; i++){
      if(i % 2 == 0){
        tong += parseInt(mavach[i]);
      }else {
        tong += parseInt(mavach[i]) * 3;
      }
    }
    tong = tong + parseInt( mavach[mavach.length - 1]);
    flag = (tong % 10 == 0)? true : false;
  }
  
  if(flag == true){
    $('.img-ma-vach').attr('src', '../img/dung.png');
    $('.table').addClass('enable_table');
    $('.check-ma-vach').text('Hợp lệ');
    $('#ma-vach-content').removeClass('bg-danger');
    $('#ma-vach-content').removeClass('bg-warning');
    $('#ma-vach-content').addClass('bg-success');
  }else if(flag == false && mavach.length > 0){
    $('.img-ma-vach').attr('src', '../img/sai.png');
    $('.check-ma-vach').text('Không hợp lệ');
    $('#ma-vach-content').removeClass('bg-warning');
    $('#ma-vach-content').removeClass('bg-success');
    $('.table').removeClass('enable_table');

    $('#ma-vach-content').addClass('bg-danger');
  }else{
    $('.img-ma-vach').attr('src', '../img/sai.png');
    $('.check-ma-vach').text('Bạn chưa nhập mã vạch');
    $('#ma-vach-content').removeClass('bg-danger');
    $('#ma-vach-content').removeClass('bg-success');
    $('.table').removeClass('enable_table');
    $('#ma-vach-content').addClass('bg-warning');
  }
})
})
// ----------- KIỂM TRA MÃ VẠCH---------

