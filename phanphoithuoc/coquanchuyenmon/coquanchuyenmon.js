var chucnang_duoc = [
{id: '1', nhiemvu: 'Kiểm tra việc hành nghề dược'}, 
{id: '2', nhiemvu: 'Xử lý vi phạm về quản lý dược và mỹ phẩm'}, 
{id: '3', nhiemvu: 'Xây dựng quy hoạch, kế hoạch phát triển công tác dược'}, 
{id: '4', nhiemvu: 'Theo dõi biến động giá thuốc, kiểm tra, kiểm soát giá thuốc'}, 
{id: '5', nhiemvu: 'Tiếp nhận và xét duyệt các hồ sơ đăng ký sản xuất thuốc'}, 

{id: '6', nhiemvu: 'Công tác dược bệnh viện, vaccin, sinh phẩm y tế'}, 
];

var kiemtra = [
	{id:'1', donvi:'Công ty TNHH dược Phúc Việt', diachi:'Số 216 đường Bãi Muối, phường Cao Thắng, thành phố Hạ Long, tỉnh THÁI BÌNH', phutrach:'DSĐH. Mai Thành Trung', sdt:'(0203) 3815221', canbo:'DS. Đỗ Văn Đông', ngay:'21/11/2018'},
	{id:'2', donvi:'Công ty TNHH dược phẩm Hồng Dương', diachi:'Số 1, tổ 1 khu 1, P.Trần Hưng Đạo, TP.Hạ Long, THÁI BÌNH', phutrach:'DSĐH. Nguyễn Thị Tiện', sdt:'(0203) 3845271', canbo:'DS. Đỗ Văn Đông', ngay:'23/11/2018'},
	{id:'3', donvi:'Công ty TNHHTM Mỹ Hoa', diachi:'Số 30- Tô Hiến Thành, P. Trần Hưng Đạo. Hạ Long QN', phutrach:'Phùng Thơ', sdt:'(0203) 3815829', canbo:'DS. Đỗ Văn Đông', ngay:'26/11/2018'},
	{id:'4', donvi:'Công ty TNHHMTV Thảo Châm', diachi:'SN 789 Đường Nguyễn Văn Cừ- P.Hồng Hải TP. HL', phutrach:'Đặng Thị Phương Châm', sdt:'(0203) 3815241', canbo:'DS. Đỗ Văn Đông', ngay:'21/11/2018'},
	{id:'5', donvi:'Công ty TNHHDP Hạ Long', diachi:'Số 08 Phố Hoàng Long, P.Bạch Đằng, TPHL', phutrach:'Nguyễn Anh Dũng', sdt:'(0203) 3855231', canbo:'DSCKI. Nguyễn Tất Đạt', ngay:'24/11/2018'},

];


$(document).ready(function(){
	chucnang_duoc.forEach(function(item){
		$('#chucnang-nhiemvu-duoc').append('<div class="col-12 text-center">\
			<div class="nhiemvu-item" data-toggle="modal" data-target="#content'+item.id+'">\
			<p class="">'+item.nhiemvu+'</p>\
			</div>\
		</div>')
	});

	kiemtra.forEach(function(item){
		$('.kiemtra tbody').append('<tr>\
			<td>'+item.id+'</td>\
			<td>'+item.donvi+'</td>\
			<td>'+item.diachi+'</td>\
			<td>'+item.phutrach+'</td>\
			<td>'+item.sdt+'</td>\
			<td>'+item.canbo+'</td>\
			<td>'+item.ngay+'</td>\
		</tr>')
	});
})