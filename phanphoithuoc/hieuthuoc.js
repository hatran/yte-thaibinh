var data2 = [
 {
   "STT": 1,
   "Name": "NT Cẩm Hưng",
   "address": "Xã Định An, Huyện Gò Quao,Tỉnh Tỉnh Thái Bình",
   "Longtitude": 9.7833747,
   "Latitude": 105.3248269
 },
 {
   "STT": 2,
   "Name": "NT Giang Nam",
   "address": "35 Trần Phú, Thành phố Rạch Giá, Tỉnh Tỉnh Thái Bình",
   "Longtitude": 10.0160691,
   "Latitude": 105.0813492
 },
 {
   "STT": 3,
   "Name": "NT Nhơn Dân",
   "address": "06 Duy Tân, Thành Phố Rạch Giá, Tỉnh Tỉnh Thái Bình",
   "Longtitude": 10.0108573,
   "Latitude": 105.0847169
 },
 {
   "STT": 4,
   "Name": "NT Nguyệt Ái",
   "address": "184 Đống Đa, Thành Phố Rạch Giá, Tỉnh Tỉnh Thái Bình",
   "Longtitude": 9.99244169999999,
   "Latitude": 105.0917122
 },
 {
   "STT": 5,
   "Name": "NT Nguyễn Mến",
   "address": "580 Nguyễn Trung Trực, Thành phố Rạch Giá, Tỉnh Tỉnh Thái Bình",
   "Longtitude": 9.9904581,
   "Latitude": 105.0971202
 },
 {
   "STT": 6,
   "Name": "NT Anh Thư",
   "address": "126 Nguyễn An Ninh, Thành phố Thành phố Rạch Giá,  Tỉnh Thái Bình",
   "Longtitude": 9.9995958,
   "Latitude": 105.0885587
 },
 {
   "STT": 7,
   "Name": "NT Tiến Phát",
   "address": "442 Nguyễn Trung Trực, Thành phố Rạch Giá, Tỉnh Thái Bình",
   "Longtitude": 9.9942054,
   "Latitude": 105.0942405
 },
 {
   "STT": 8,
   "Name": "NT Nguyên Trinh",
   "address": "668 Ngô Quyền, Thành phố Rạch Giá, Tỉnh Thái Bình",
   "Longtitude": 9.98601779999999,
   "Latitude": 105.1051725
 },
 {
   "STT": 9,
   "Name": "TTB Vạn Phước Đường",
   "address": "98 Mạc Thiên Tích, Thị xã Hà Tiên, Tỉnh Thái Bình",
   "Longtitude": 10.3815665,
   "Latitude": 104.4851498
 },
 {
   "STT": 10,
   "Name": "NT Đăng Trâm",
   "address": "232 Trần Phú, Thành phố Rạch Giá, Tỉnh Thái Bình",
   "Longtitude": 10.00987,
   "Latitude": 105.086
 },
 {
   "STT": 11,
   "Name": "NT Nguyệt Ái",
   "address": "184-186 Đống Đa, Thành phố Rạch Giá, Tỉnh Thái Bình",
   "Longtitude": 9.99244169999999,
   "Latitude": 105.0917122
 },
 {
   "STT": 12,
   "Name": "Nhà Thuốc Minh Phương",
   "address": "Phường Rạch Sỏi, Thành phố Rạch Giá, Tỉnh Thái Bình",
   "Longtitude": 9.9483946,
   "Latitude": 105.1156893
 },
 {
   "STT": 13,
   "Name": "Nguyệt Ái",
   "address": "Phường Vĩnh Lạc, Thành phố Rạch Giá, Tỉnh Thái Bình",
   "Longtitude": 9.9943793,
   "Latitude": 105.0943227
 },
 {
   "STT": 14,
   "Name": "Trân Thành",
   "address": "5 Mạc Cửu, Phường Vĩnh Thanh, Thành phố Rạch Giá, Tỉnh Thái Bình",
   "Longtitude": 10.0138315,
   "Latitude": 105.083592
 },
 {
   "STT": 15,
   "Name": "Nguyễn Mếm",
   "address": "580 Nguyễn Trung Trực, Phường Vĩnh Lạc, Thành phố Rạch Giá, Tỉnh Thái Bình",
   "Longtitude": 9.9904581,
   "Latitude": 105.0971202
 },
 {
   "STT": 16,
   "Name": "Nhân Văn",
   "address": "7 Duy Tân, Phường Vĩnh Lạc, Thành phố Rạch Giá, Tỉnh Thái Bình",
   "Longtitude": 10.0107536,
   "Latitude": 105.0846487
 },
 {
   "STT": 17,
   "Name": "Tế Dân",
   "address": "17 Duy Tân, Phường Vĩnh Lạc, Thành phố Rạch Giá, Tỉnh Thái Bình",
   "Longtitude": 10.0106961,
   "Latitude": 105.0843473
 },
 {
   "STT": 18,
   "Name": "Tân Đại Phong",
   "address": "1 Hoàng Diệu, Phường Vĩnh Lạc,  Thành phố Rạch Giá, Tỉnh Thái Bình",
   "Longtitude": 10.0105109,
   "Latitude": 105.0796132
 },
 {
   "STT": 19,
   "Name": "Bửu Thành",
   "address": " Phường Vĩnh Lợi, Thị xã Rạch Sỏi, Tỉnh Thái Bình",
   "Longtitude": 9.9552245,
   "Latitude": 105.1236923
 },
 {
   "STT": 20,
   "Name": "NT Nhơn Dân",
   "address": "49 Duy Tân, phường Vĩnh Thanh Vân, Thành phố Rạch Giá, Tỉnh Thái Bình",
   "Longtitude": 10.0105646,
   "Latitude": 105.0834015
 },
 {
   "STT": 21,
   "Name": "NT Tế Dân",
   "address": "17 Duy Tân,  phường Vĩnh Thanh Vân, Thành phố Rạch Giá, Tỉnh Thái Bình",
   "Longtitude": 10.0106961,
   "Latitude": 105.0843473
 },
 {
   "STT": 22,
   "Name": "NT Ngọc Thảo",
   "address": "Phường Vĩnh Thanh, Thành phố Rạch Giá, Tỉnh Thái Bình",
   "Longtitude": 10.0145123,
   "Latitude": 105.0818276
 },
 {
   "STT": 23,
   "Name": "NT Giang Nam",
   "address": "Phường Vĩnh Thanh, Thành phố Rạch Giá, Tỉnh Thái Bình\n",
   "Longtitude": 10.0145123,
   "Latitude": 105.0818276
 },
 {
   "STT": 24,
   "Name": "NT Mai Anh",
   "address": " 61 Lê Lợi, Phường Vĩnh Thanh Vân, Thành phố Rạch Giá, tỉnh Thái Bình",
   "Longtitude": 10.0088234,
   "Latitude": 105.0838404
 },
 {
   "STT": 25,
   "Name": "NT Ngọc Anh",
   "address": "198 Nguyễn Bỉnh Khiêm, Phường Vĩnh Thanh, Thành phố Rạch Giá, Tỉnh Thái Bình",
   "Longtitude": 10.0190794,
   "Latitude": 105.0853704
 },
 {
   "STT": 26,
   "Name": "NT Lâm Linh",
   "address": "Phường An Hoà, Thành phố Rạch Giá, Tỉnh Thái Bình",
   "Longtitude": 9.9804585,
   "Latitude": 105.1043935
 },
 {
   "STT": 27,
   "Name": "NT Trung Nguyên",
   "address": "57 Lê Lợi, Phường Vĩnh Thanh Vân, Thành phố Rạch Giá, Tỉnh Thái Bình.",
   "Longtitude": 10.0088787,
   "Latitude": 105.0838167
 },
 {
   "STT": 28,
   "Name": "Tiệm thuốc bắc Ích An Đường",
   "address": "Phường Vĩnh Thanh, Thành phố Rạch Giá, Tỉnh Thái Bình",
   "Longtitude": 10.0145123,
   "Latitude": 105.0818276
 },
 {
   "STT": 29,
   "Name": "NT Thành Đạt",
   "address": "421 Ngô Quyền, Phường Vĩnh lạc,Thành phố Rạch Giá, Tỉnh Thái Bình.",
   "Longtitude": 10.0008026,
   "Latitude": 105.0934539
 },
 {
   "STT": 30,
   "Name": "PCTYHCT Đức Hưng",
   "address": "315 Nguyễn Trung Trực,Phường Vĩnh lạc,Thành phố Rạch Giá, Tỉnh Thái Bình.",
   "Longtitude": 9.99917829999999,
   "Latitude": 105.0907903
 },
 {
   "STT": 31,
   "Name": "PCTYHCT Tân Đại Phong",
   "address": "D5 Nguyễn Tri Phương, Phường Vĩnh Quang,Thành phố Rạch Giá, Tỉnh Thái Bình.",
   "Longtitude": 10.0193316,
   "Latitude": 105.0812758
 },
 {
   "STT": 32,
   "Name": "NT Ngọc Hằng",
   "address": "40 Trần Quang Khải , Thành phố Rạch Giá, Tỉnh Thái Bình.",
   "Longtitude": 9.9851727,
   "Latitude": 105.1044184
 },
 {
   "STT": 33,
   "Name": "NT Nam Nguyên",
   "address": " 359A Lâm Quang Ky, Phường Vĩnh Lạc, Thành phố Rạch Giá, Tỉnh Thái Bình.",
   "Longtitude": 9.99072419999999,
   "Latitude": 105.0924632
 }
];