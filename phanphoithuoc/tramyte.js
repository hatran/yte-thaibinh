var datatramyte = [
 {
   "STT": 1,
   "Name": "Trạm y tế phường Lê Hồng Phong",
   "address": "Phường Lê Hồng Phong, TP Thái Bình",
   "Longtitude": 20.451534,
   "Latitude": 106.3456432
 },
 {
   "STT": 2,
   "Name": "Trạm y tế phường Bồ Xuyên",
   "address": "Phường Bồ Xuyên, TP Thái Bình",
   "Longtitude": 20.4555003,
   "Latitude": 106.3419641
 },
 {
   "STT": 3,
   "Name": "Trạm y tế phường Đề Thám",
   "address": "Phường Đề Thám, TP Thái Bình",
   "Longtitude": 20.4464594,
   "Latitude": 106.3382851
 },
 {
   "STT": 4,
   "Name": "Trạm y tế phường Kỳ Bá",
   "address": "Phường Kỳ Bá, TP Thái Bình",
   "Longtitude": 20.4418034,
   "Latitude": 106.3425334
 },
 {
   "STT": 5,
   "Name": "Trạm y tế phường Quang Trung",
   "address": "Phường Quang Trung, TP Thái Bình",
   "Longtitude": 20.4373541,
   "Latitude": 106.3319534
 },
 {
   "STT": 6,
   "Name": "Trạm y tế phường Phú Khánh",
   "address": "Phường Phú Khánh, TP Thái Bình",
   "Longtitude": 20.4384961,
   "Latitude": 106.3123186
 },
 {
   "STT": 7,
   "Name": "Trạm y tế phường Tiền Phong",
   "address": "Phường Tiền Phong, TP Thái Bình",
   "Longtitude": 20.4544845,
   "Latitude": 106.334125
 },
 {
   "STT": 8,
   "Name": "Trạm y tế phường Trần Lãm",
   "address": "Phường Trần Lãm, TP Thái Bình",
   "Longtitude": 20.4415905,
   "Latitude": 106.3561327
 },
 {
   "STT": 9,
   "Name": "Trạm y tế xã Đông Hòa",
   "address": "Xã Đông Hòa, TP Thái Bình ",
   "Longtitude": 20.4800306,
   "Latitude": 106.3495193
 },
 {
   "STT": 10,
   "Name": "Trạm y tế xã Hoàng Diệu",
   "address": "Xã Hoàng Diệu,  TP Thái Bình",
   "Longtitude": 20.4670122,
   "Latitude": 106.3587079
 },
 {
   "STT": 11,
   "Name": "Trạm y tế xã Phú Xuân",
   "address": "Xã Phú Xuân, TP Thái Bình ",
   "Longtitude": 20.4549191,
   "Latitude": 106.3257257
 },
 {
   "STT": 12,
   "Name": "Trạm y tế xã Vũ Phúc",
   "address": "Xã Vũ Phúc, TP Thái Bình",
   "Longtitude": 20.4134629,
   "Latitude": 106.32361
 },
 {
   "STT": 13,
   "Name": "Trạm y tế xã Vũ Chính",
   "address": "Xã Vũ Chính, TP Thái Bình",
   "Longtitude": 20.4278977,
   "Latitude": 106.3554272
 },
 {
   "STT": 14,
   "Name": "Trạm y tế thị trấn Quỳnh Côi",
   "address": "Thị Trấn Quỳnh Côi, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6511779,
   "Latitude": 106.3213623
 },
 {
   "STT": 15,
   "Name": "Trạm y tế xã An Khê",
   "address": "Xã An Khê, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.7135825,
   "Latitude": 106.4081983
 },
 {
   "STT": 16,
   "Name": "Trạm y tế xã An Đồng",
   "address": "Xã An Đồng, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6008161,
   "Latitude": 106.3787583
 },
 {
   "STT": 17,
   "Name": "Trạm y tế xã Quỳnh Hoa",
   "address": "Xã Quỳnh Hoa, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6920251,
   "Latitude": 106.325041
 },
 {
   "STT": 18,
   "Name": "Trạm y tế xã Quỳnh Lâm",
   "address": "Xã Quỳnh Lâm, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6925806,
   "Latitude": 106.2669247
 },
 {
   "STT": 19,
   "Name": "Trạm y tế xã Quỳnh Thọ",
   "address": "Xã Quỳnh Thọ, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6898244,
   "Latitude": 106.3552092
 },
 {
   "STT": 20,
   "Name": "Trạm y tế xã An Hiệp",
   "address": "Xã An Hiệp, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6889489,
   "Latitude": 106.3751301
 },
 {
   "STT": 21,
   "Name": "Trạm y tế xã Quỳnh Hoàng",
   "address": "Xã Quỳnh Hoàng, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6817133,
   "Latitude": 106.2836735
 },
 {
   "STT": 22,
   "Name": "Trạm y tế xã Quỳnh Giao",
   "address": "Xã Quỳnh Giao, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6871409,
   "Latitude": 106.3078328
 },
 {
   "STT": 23,
   "Name": "Trạm y tế xã An Thái",
   "address": "Xã An Thái, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6789745,
   "Latitude": 106.385049
 },
 {
   "STT": 24,
   "Name": "Trạm y tế xã An Cầu",
   "address": "Xã An Cầu, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.66905,
   "Latitude": 106.3905338
 },
 {
   "STT": 25,
   "Name": "Trạm y tế xã Quỳnh Hồng",
   "address": "Xã Quỳnh Hồng, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6695452,
   "Latitude": 106.3257768
 },
 {
   "STT": 26,
   "Name": "Trạm y tế xã Quỳnh Khê",
   "address": "Xã Quỳnh Khê, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6723894,
   "Latitude": 106.2963486
 },
 {
   "STT": 27,
   "Name": "Trạm y tế xã Quỳnh Minh",
   "address": "Xã Quỳnh Minh, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6719069,
   "Latitude": 106.3610962
 },
 {
   "STT": 28,
   "Name": "Trạm y tế xã An Ninh",
   "address": "Xã An Ninh, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6631423,
   "Latitude": 106.4155342
 },
 {
   "STT": 29,
   "Name": "Trạm y tế xã Quỳnh Ngọc",
   "address": "Xã Quỳnh Ngọc, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6636599,
   "Latitude": 106.2669247
 },
 {
   "STT": 30,
   "Name": "Trạm y tế xã Quỳnh Hải",
   "address": "Xã Quỳnh Hải, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6568409,
   "Latitude": 106.3375493
 },
 {
   "STT": 31,
   "Name": "Trạm y tế thị trấn An Bài",
   "address": "Thị Trấn An Bài, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6482726,
   "Latitude": 106.4258643
 },
 {
   "STT": 32,
   "Name": "Trạm y tế xã An ấp",
   "address": "Xã An ấp, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6488987,
   "Latitude": 106.3762164
 },
 {
   "STT": 33,
   "Name": "Trạm y tế xã Quỳnh Hội",
   "address": "Xã Quỳnh Hội, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6519347,
   "Latitude": 106.35394
 },
 {
   "STT": 34,
   "Name": "Trạm y tế xã Quỳnh Sơn",
   "address": "Xã Quỳnh Sơn, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6518628,
   "Latitude": 106.3018817
 },
 {
   "STT": 35,
   "Name": "Trạm y tế xã Quỳnh Mỹ",
   "address": "Xã Quỳnh Mỹ, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6475515,
   "Latitude": 106.314005
 },
 {
   "STT": 36,
   "Name": "Trạm y tế Xã An Quý",
   "address": "Xã An Quý, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6465088,
   "Latitude": 106.3844373
 },
 {
   "STT": 37,
   "Name": "Trạm y tế xã An Thanh",
   "address": "Xã An Thanh, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.636701,
   "Latitude": 106.4415782
 },
 {
   "STT": 38,
   "Name": "Trạm y tế xã Quỳnh Châu",
   "address": "Xã Quỳnh Châu, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6446074,
   "Latitude": 106.2845785
 },
 {
   "STT": 39,
   "Name": "Trạm y tế xã An Vũ",
   "address": "Xã An Vũ, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6164479,
   "Latitude": 106.3964218
 },
 {
   "STT": 40,
   "Name": "Trạm y tế xã An Lễ",
   "address": "Xã An Lễ, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.628581,
   "Latitude": 106.3905338
 },
 {
   "STT": 41,
   "Name": "Trạm y tế xã Quỳnh Hưng",
   "address": "Xã Quỳnh Hưng, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6287294,
   "Latitude": 106.3375444
 },
 {
   "STT": 42,
   "Name": "Trạm y tế xã Quỳnh Bảo",
   "address": "Xã Quỳnh Bảo, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6241159,
   "Latitude": 106.3190136
 },
 {
   "STT": 43,
   "Name": "Trạm y tế xã An Mỹ",
   "address": "Xã An Mỹ, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6176529,
   "Latitude": 106.4435319
 },
 {
   "STT": 44,
   "Name": "Trạm y tế xã Quỳnh Nguyên",
   "address": "Xã Quỳnh Nguyên, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6197725,
   "Latitude": 106.3022339
 },
 {
   "STT": 45,
   "Name": "Trạm y tế xã An Vinh",
   "address": "Xã An Vinh, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6187301,
   "Latitude": 106.37287069999999
 },
 {
   "STT": 46,
   "Name": "Trạm y tế xã Quỳnh Xá",
   "address": "Xã Quỳnh Xá, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6210088,
   "Latitude": 106.3493224
 },
 {
   "STT": 47,
   "Name": "Trạm y tế xã An Dục",
   "address": "Xã An Dục, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6141622,
   "Latitude": 106.4199755
 },
 {
   "STT": 48,
   "Name": "Trạm y tế xã Đông Hải",
   "address": "Xã Đông Hải, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6008161,
   "Latitude": 106.3787583
 },
 {
   "STT": 49,
   "Name": "Trạm y tế xã Quỳnh Trang",
   "address": "Xã Quỳnh Trang, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6042317,
   "Latitude": 106.3434358
 },
 {
   "STT": 50,
   "Name": "Trạm y tế xã An Tràng",
   "address": "Xã An Tràng, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.5973925,
   "Latitude": 106.4140868
 },
 {
   "STT": 51,
   "Name": "Trạm y tế Xã Đồng Tiến",
   "address": "Xã Đồng Tiến, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.5835461,
   "Latitude": 106.4376425
 },
 {
   "STT": 52,
   "Name": "Trạm y tế thị trấn Hưng Hà",
   "address": "Thị Trấn Hưng Hà, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.5929854,
   "Latitude": 106.2198553
 },
 {
   "STT": 53,
   "Name": "Trạm y tế xã Điệp Nông",
   "address": "Xã Điệp Nông, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.6549225,
   "Latitude": 106.237505
 },
 {
   "STT": 54,
   "Name": "Trạm y tế xã Tân Lễ",
   "address": "Xã Tân Lễ, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.6497575,
   "Latitude": 106.140451
 },
 {
   "STT": 55,
   "Name": "Trạm y tế xã Cộng Hòa",
   "address": "Xã Cộng Hòa, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.6426464,
   "Latitude": 106.1845606
 },
 {
   "STT": 56,
   "Name": "Trạm y tế xã Dân Chủ",
   "address": "Xã Dân Chủ, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.6463079,
   "Latitude": 106.2669247
 },
 {
   "STT": 57,
   "Name": "Trạm y tế xã Canh Tân",
   "address": "Xã Canh Tân, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.6396734,
   "Latitude": 106.1551531
 },
 {
   "STT": 58,
   "Name": "Trạm y tế Xã HòA Tiến",
   "address": "Xã Hòa Tiến, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.6299489,
   "Latitude": 106.1963248
 },
 {
   "STT": 59,
   "Name": "Trạm y tế xã Hùng Dũng",
   "address": "Xã Hùng Dũng, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.6306538,
   "Latitude": 106.2492724
 },
 {
   "STT": 60,
   "Name": "Trạm y tế xã Tân Tiến",
   "address": "Xã Tân Tiến, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.6224728,
   "Latitude": 106.2139724
 },
 {
   "STT": 61,
   "Name": "Trạm y tế thị trấn Hưng Nhân",
   "address": "Thị Trấn Hưng Nhân, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.617651,
   "Latitude": 106.1433913
 },
 {
   "STT": 62,
   "Name": "Trạm y tế xã Đoan Hùng",
   "address": "Xã Đoan Hùng, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.6207801,
   "Latitude": 106.2316216
 },
 {
   "STT": 63,
   "Name": "Trạm y tế xã Duyên Hải",
   "address": "Xã Duyên Hải, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.6341738,
   "Latitude": 106.2728091
 },
 {
   "STT": 64,
   "Name": "Trạm y tế xã Tân Hòa",
   "address": "Xã Tân Hòa, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.6275391,
   "Latitude": 106.1610343
 },
 {
   "STT": 65,
   "Name": "Trạm y tế xã Văn Cẩm",
   "address": "Xã Văn Cẩm, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.6173888,
   "Latitude": 106.2669247
 },
 {
   "STT": 66,
   "Name": "Trạm y tế xã Bắc Sơn",
   "address": "Xã Bắc Sơn, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.6151235,
   "Latitude": 106.2904635
 },
 {
   "STT": 67,
   "Name": "Trạm y tế xã Đông Đô",
   "address": "Xã Đông Đô, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.6035571,
   "Latitude": 106.2904635
 },
 {
   "STT": 68,
   "Name": "Trạm y tế xã Phúc Khánh",
   "address": "Xã Phúc Khánh, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.6068078,
   "Latitude": 106.1963248
 },
 {
   "STT": 69,
   "Name": "Trạm y tế xã Liên Hiệp",
   "address": "Xã Liên Hiệp, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.6106444,
   "Latitude": 106.1713267
 },
 {
   "STT": 70,
   "Name": "Trạm y tế xã Tây Đô",
   "address": "Xã Tây Đô, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.593123,
   "Latitude": 106.2786937
 },
 {
   "STT": 71,
   "Name": "Trạm y tế xã Thống Nhất",
   "address": "Xã Thống Nhất, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.602298,
   "Latitude": 106.2433886
 },
 {
   "STT": 72,
   "Name": "Trạm y tế Xã Tiến Đức",
   "address": "Xã Tiến Đức, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.5945058,
   "Latitude": 106.1433913
 },
 {
   "STT": 73,
   "Name": "Trạm y tế xã Thái Hưng",
   "address": "Xã Thái Hưng, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.5917006,
   "Latitude": 106.1727971
 },
 {
   "STT": 74,
   "Name": "Trạm y tế xã Thái Phương",
   "address": "Xã Thái Phương, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.5900148,
   "Latitude": 106.1904426
 },
 {
   "STT": 75,
   "Name": "Trạm y tế xã Bình Lăng",
   "address": "Xã Bình Lăng, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.5763385,
   "Latitude": 106.2728091
 },
 {
   "STT": 76,
   "Name": "Trạm y tế xã Minh Khai",
   "address": "Xã Minh Khai, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.5797257,
   "Latitude": 106.237505
 },
 {
   "STT": 77,
   "Name": "Trạm y tế xã Hồng An",
   "address": "Xã Hồng An, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.5791998,
   "Latitude": 106.1522126
 },
 {
   "STT": 78,
   "Name": "Trạm y tế xã Kim Trung",
   "address": "Xã Kim Trung, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.5767576,
   "Latitude": 106.2080897
 },
 {
   "STT": 79,
   "Name": "Trạm y tế xã Hồng Lĩnh",
   "address": "Xã Hồng Lĩnh, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.5670299,
   "Latitude": 106.2492724
 },
 {
   "STT": 80,
   "Name": "Trạm y tế xã Minh Tân",
   "address": "Xã Minh Tân, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.567436,
   "Latitude": 106.1845606
 },
 {
   "STT": 81,
   "Name": "Trạm y tế xã Văn Lang",
   "address": "Xã Văn Lang, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.564063,
   "Latitude": 106.2198553
 },
 {
   "STT": 82,
   "Name": "Trạm y tế xã Độc Lập",
   "address": "Xã Độc Lập, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.5495195,
   "Latitude": 106.1904426
 },
 {
   "STT": 83,
   "Name": "Trạm y tế Xã Chí Hòa",
   "address": "Xã Chí Hòa, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.5366275,
   "Latitude": 106.2345633
 },
 {
   "STT": 84,
   "Name": "Trạm y tế xã Minh Hòa",
   "address": "Xã Minh Hòa, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.5388769,
   "Latitude": 106.2110311
 },
 {
   "STT": 85,
   "Name": "Trạm y tế xã Hồng Minh",
   "address": "Xã Hồng Minh, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.5252582,
   "Latitude": 106.2022072
 },
 {
   "STT": 86,
   "Name": "Trạm y tế xã Hoà Bình",
   "address": "Xã Hoà Bình, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.5896006,
   "Latitude": 106.2551563
 },
 {
   "STT": 87,
   "Name": "Trạm y tế xã Chi Lăng",
   "address": "Xã Chi Lăng, Huyện Hưng Hà, Tỉnh Thái Bình",
   "Longtitude": 20.5763385,
   "Latitude": 106.2728091
 },
 {
   "STT": 88,
   "Name": "Trạm y tế thị trấn Đông Hưng",
   "address": "Thị Trấn Đông Hưng, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5561898,
   "Latitude": 106.3544734
 },
 {
   "STT": 89,
   "Name": "Trạm y tế xã Đô Lương",
   "address": "Xã Đô Lương, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.6001542,
   "Latitude": 106.3257768
 },
 {
   "STT": 90,
   "Name": "Trạm y tế xã Đông Phương",
   "address": "Xã Đông Phương, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5875437,
   "Latitude": 106.3964218
 },
 {
   "STT": 91,
   "Name": "Trạm y tế xã Liên Giang",
   "address": "Xã Liên Giang, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5868859,
   "Latitude": 106.3434358
 },
 {
   "STT": 92,
   "Name": "Trạm y tế xã An Châu",
   "address": "Xã An Châu, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5897242,
   "Latitude": 106.314005
 },
 {
   "STT": 93,
   "Name": "Trạm y tế xã Đông Sơn",
   "address": "Xã Đông Sơn, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5808668,
   "Latitude": 106.3758145
 },
 {
   "STT": 94,
   "Name": "Trạm y tế xã Đông Cường",
   "address": "Xã Đông Cường, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5647448,
   "Latitude": 106.4229199
 },
 {
   "STT": 95,
   "Name": "Trạm y tế xã Phú Lương",
   "address": "Xã Phú Lương, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5712429,
   "Latitude": 106.3257768
 },
 {
   "STT": 96,
   "Name": "Trạm y tế xã Mê Linh",
   "address": "Xã Mê Linh, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5729434,
   "Latitude": 106.3081194
 },
 {
   "STT": 97,
   "Name": "Trạm y tế xã Lô Giang",
   "address": "Xã Lô Giang, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5578587,
   "Latitude": 106.2845785
 },
 {
   "STT": 98,
   "Name": "Trạm y tế xã Đông La",
   "address": "Xã Đông La, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.567836,
   "Latitude": 106.3610962
 },
 {
   "STT": 99,
   "Name": "Trạm y tế xã Minh Tân",
   "address": "Xã Minh Tân, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5568902,
   "Latitude": 106.2750281
 },
 {
   "STT": 100,
   "Name": "Trạm y tế xã Đông Xá",
   "address": "Xã Đông Xá, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5644211,
   "Latitude": 106.3964218
 },
 {
   "STT": 101,
   "Name": "Trạm y tế xã Chương Dương",
   "address": "Xã Chương Dương, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.544597,
   "Latitude": 106.3022339
 },
 {
   "STT": 102,
   "Name": "Trạm y tế xã Nguyên Xá",
   "address": "Xã Nguyên Xá, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5521954,
   "Latitude": 106.3434358
 },
 {
   "STT": 103,
   "Name": "Trạm y tế xã Phong Châu",
   "address": "Xã Phong Châu, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5538967,
   "Latitude": 106.3257768
 },
 {
   "STT": 104,
   "Name": "Trạm y tế Xã Hợp Tiến",
   "address": "Xã Hợp Tiến, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5569001,
   "Latitude": 106.3095908
 },
 {
   "STT": 105,
   "Name": "Trạm y tế xã Hồng Việt",
   "address": "Xã Hồng Việt, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5459414,
   "Latitude": 106.2580983
 },
 {
   "STT": 106,
   "Name": "Trạm y tế xã Đông Hà",
   "address": "Xã Đông Hà, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5354529,
   "Latitude": 106.3839025
 },
 {
   "STT": 107,
   "Name": "Trạm y tế xã Đông Giang",
   "address": "Xã Đông Giang, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5392066,
   "Latitude": 106.3920321
 },
 {
   "STT": 108,
   "Name": "Trạm y tế xã Đông Kinh",
   "address": "Xã Đông Kinh, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5448002,
   "Latitude": 106.4199755
 },
 {
   "STT": 109,
   "Name": "Trạm y tế xã Đông Hợp",
   "address": "Xã Đông Hợp, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5454054,
   "Latitude": 106.3553715
 },
 {
   "STT": 110,
   "Name": "Trạm y tế xã Thăng Long",
   "address": "Xã Thăng Long, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5455532,
   "Latitude": 106.2772226
 },
 {
   "STT": 111,
   "Name": "Trạm y tế xã Đông Các",
   "address": "Xã Đông Các, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5333834,
   "Latitude": 106.366358
 },
 {
   "STT": 112,
   "Name": "Trạm y tế xã Phú Châu",
   "address": "Xã Phú Châu, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5359843,
   "Latitude": 106.3316629
 },
 {
   "STT": 113,
   "Name": "Trạm y tế xã Hoa Lư",
   "address": "Xã Hoa Lư, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5327455,
   "Latitude": 106.2850902
 },
 {
   "STT": 114,
   "Name": "Trạm y tế xã Minh Châu",
   "address": "Xã Minh Châu, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.531901,
   "Latitude": 106.314005
 },
 {
   "STT": 115,
   "Name": "Trạm y tế xã Đông Tân",
   "address": "Xã Đông Tân, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5326703,
   "Latitude": 106.4258643
 },
 {
   "STT": 116,
   "Name": "Trạm y tế xã Đông Vinh",
   "address": "Xã Đông Vinh, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5242308,
   "Latitude": 106.3921038
 },
 {
   "STT": 117,
   "Name": "Trạm y tế xã Đông Động",
   "address": "Xã Đông Động, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5263478,
   "Latitude": 106.356681
 },
 {
   "STT": 118,
   "Name": "Trạm y tế xã Hồng Châu",
   "address": "Xã Hồng Châu, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5248542,
   "Latitude": 106.2669247
 },
 {
   "STT": 119,
   "Name": "Trạm y tế xã Bạch Đằng",
   "address": "Xã Bạch Đằng, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.521324,
   "Latitude": 106.2433886
 },
 {
   "STT": 120,
   "Name": "Trạm y tế xã Trọng Quan",
   "address": "Xã Trọng Quan, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5160314,
   "Latitude": 106.3287199
 },
 {
   "STT": 121,
   "Name": "Trạm y tế xã Hoa Nam",
   "address": "Xã Hoa Nam, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5173791,
   "Latitude": 106.2845785
 },
 {
   "STT": 122,
   "Name": "Trạm y tế xã Hồng Giang",
   "address": "Xã Hồng Giang, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5069416,
   "Latitude": 106.2728091
 },
 {
   "STT": 123,
   "Name": "Trạm y tế xã Đông Phong",
   "address": "Xã Đông Phong, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5122781,
   "Latitude": 106.4126147
 },
 {
   "STT": 124,
   "Name": "Trạm y tế xã Đông Quang",
   "address": "Xã Đông Quang, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5141176,
   "Latitude": 106.3546085
 },
 {
   "STT": 125,
   "Name": "Trạm y tế xã Đông Xuân",
   "address": "Xã Đông Xuân, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5094572,
   "Latitude": 106.3669834
 },
 {
   "STT": 126,
   "Name": "Trạm y tế xã Đông á",
   "address": "Xã Đông á, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.4976631,
   "Latitude": 106.3993658
 },
 {
   "STT": 127,
   "Name": "Trạm y tế xã Đông Lĩnh",
   "address": "Xã Đông Lĩnh, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.508982,
   "Latitude": 106.4317533
 },
 {
   "STT": 128,
   "Name": "Trạm y tế xã Đông Hoàng",
   "address": "Xã Đông Hoàng, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.4961933,
   "Latitude": 106.3846459
 },
 {
   "STT": 129,
   "Name": "Trạm y tế xã Đông Dương",
   "address": "Xã Đông Dương, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5059437,
   "Latitude": 106.3434358
 },
 {
   "STT": 130,
   "Name": "Trạm y tế xã Đông Huy",
   "address": "Xã Đông Huy, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.4992552,
   "Latitude": 106.421416
 },
 {
   "STT": 131,
   "Name": "Trạm y tế xã Đông Mỹ",
   "address": "Xã Đông Mỹ, TP Thái Bình",
   "Longtitude": 20.4921149,
   "Latitude": 106.3669834
 },
 {
   "STT": 132,
   "Name": "Trạm y tế xã Đông Thọ",
   "address": "Xã Đông Thọ, TP Thái Bình",
   "Longtitude": 20.4918552,
   "Latitude": 106.3443466
 },
 {
   "STT": 133,
   "Name": "Trạm y tế xã Đồng Phú",
   "address": "Xã Đồng Phú, Huyện Đông Hưng, Tỉnh Thái Bình",
   "Longtitude": 20.5156852,
   "Latitude": 106.3022339
 },
 {
   "STT": 134,
   "Name": "Trạm y tế thị trấn Diêm Điền",
   "address": "Thị Trấn Diêm Điền, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5607683,
   "Latitude": 106.5673279
 },
 {
   "STT": 135,
   "Name": "Trạm y tế xã Thụy Tân",
   "address": "Xã Thụy Tân, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.62579,
   "Latitude": 106.5967126
 },
 {
   "STT": 136,
   "Name": "Trạm y tế xã Thụy Trường",
   "address": "Xã Thụy Trường, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.6145177,
   "Latitude": 106.6232359
 },
 {
   "STT": 137,
   "Name": "Trạm y tế xã Hồng Quỳnh",
   "address": "Xã Hồng Quỳnh, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.6182835,
   "Latitude": 106.5554608
 },
 {
   "STT": 138,
   "Name": "Trạm y tế xã Thụy Dũng",
   "address": "Xã Thụy Dòng, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.6044186,
   "Latitude": 106.5790323
 },
 {
   "STT": 139,
   "Name": "Trạm y tế xã Thụy Hồng",
   "address": "Xã Thụy Hồng, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5997969,
   "Latitude": 106.5672462
 },
 {
   "STT": 140,
   "Name": "Trạm y tế xã Thụy Quỳnh",
   "address": "Xã Thụy Quỳnh, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.6021046,
   "Latitude": 106.543676
 },
 {
   "STT": 141,
   "Name": "Trạm y tế xã Thụy An",
   "address": "Xã Thụy An, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.6032623,
   "Latitude": 106.590819
 },
 {
   "STT": 142,
   "Name": "Trạm y tế xã Thụy Ninh",
   "address": "Xã Thụy Ninh, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5980217,
   "Latitude": 106.4670909
 },
 {
   "STT": 143,
   "Name": "Trạm y tế xã Thụy Hưng",
   "address": "Xã Thụy Hưng, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5893735,
   "Latitude": 106.4965434
 },
 {
   "STT": 144,
   "Name": "Trạm y tế xã Thụy Việt",
   "address": "Xã Thụy Việt, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.6029578,
   "Latitude": 106.50538
 },
 {
   "STT": 145,
   "Name": "Trạm y tế xã Thụy Văn",
   "address": "Xã Thụy Văn, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5870745,
   "Latitude": 106.5201084
 },
 {
   "STT": 146,
   "Name": "Trạm y tế xã Thụy Xuân",
   "address": "Xã Thụy Xuân, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5899738,
   "Latitude": 106.6085003
 },
 {
   "STT": 147,
   "Name": "Trạm y tế xã Thụy Dương",
   "address": "Xã Thụy Dương, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5703152,
   "Latitude": 106.5142169
 },
 {
   "STT": 148,
   "Name": "Trạm y tế xã Thụy Trình",
   "address": "Xã Thụy Trình, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5771723,
   "Latitude": 106.5626973
 },
 {
   "STT": 149,
   "Name": "Trạm y tế xã Thụy Bình",
   "address": "Xã Thụy Bình, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5691656,
   "Latitude": 106.5260001
 },
 {
   "STT": 150,
   "Name": "Trạm y tế xã Thụy Chính",
   "address": "Xã Thụy Chính, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5754776,
   "Latitude": 106.4612009
 },
 {
   "STT": 151,
   "Name": "Trạm y tế xã Thụy Dân",
   "address": "Xã Thụy Dân, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5737588,
   "Latitude": 106.4788714
 },
 {
   "STT": 152,
   "Name": "Trạm y tế xã Thụy Hải",
   "address": "Xã Thụy Hải, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.562827,
   "Latitude": 106.590819
 },
 {
   "STT": 153,
   "Name": "Trạm y tế xã Thụy Phúc",
   "address": "Xã Thụy Phúc, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5704499,
   "Latitude": 106.4980162
 },
 {
   "STT": 154,
   "Name": "Trạm y tế xã Thụy Lương",
   "address": "Xã Thụy Lương, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5755347,
   "Latitude": 106.5790323
 },
 {
   "STT": 155,
   "Name": "Trạm y tế xã Thụy Liên",
   "address": "Xã Thụy Liên, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5570352,
   "Latitude": 106.5318919
 },
 {
   "STT": 156,
   "Name": "Trạm y tế xã Thụy Duyên",
   "address": "Xã Thụy Duyên, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5639192,
   "Latitude": 106.4612009
 },
 {
   "STT": 157,
   "Name": "Trạm y tế xã Thụy Hà",
   "address": "Xã Thụy Hà, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5553093,
   "Latitude": 106.5495683
 },
 {
   "STT": 158,
   "Name": "Trạm y tế xã Thụy Thanh",
   "address": "Xã Thụy Thanh, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5540762,
   "Latitude": 106.4435319
 },
 {
   "STT": 159,
   "Name": "Trạm y tế xã Thụy Sơn",
   "address": "Xã Thụy Sơn, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5489245,
   "Latitude": 106.4965434
 },
 {
   "STT": 160,
   "Name": "Trạm y tế xã Thụy Phong",
   "address": "Xã Thụy Phong, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5454376,
   "Latitude": 106.4729811
 },
 {
   "STT": 161,
   "Name": "Trạm y tế xã Thái Thượng",
   "address": "Xã Thái Thượng, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5382754,
   "Latitude": 106.5760857
 },
 {
   "STT": 162,
   "Name": "Trạm y tế xã Thái Nguyên",
   "address": "Xã Thái Nguyên, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5281479,
   "Latitude": 106.5318919
 },
 {
   "STT": 163,
   "Name": "Trạm y tế xã Thái Thủy",
   "address": "Xã Thái Thuỷ, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5356476,
   "Latitude": 106.5142169
 },
 {
   "STT": 164,
   "Name": "Trạm y tế xã Thái Dương",
   "address": "Xã Thái Dương, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5263845,
   "Latitude": 106.4906526
 },
 {
   "STT": 165,
   "Name": "Trạm y tế xã Thái Giang",
   "address": "Xã Thái Giang, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5236738,
   "Latitude": 106.4511003
 },
 {
   "STT": 166,
   "Name": "Trạm y tế xã Thái Hoà",
   "address": "Xã Thái Hoà, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5142953,
   "Latitude": 106.5554608
 },
 {
   "STT": 167,
   "Name": "Trạm y tế xã Thái Sơn",
   "address": "Xã Thái Sơn, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5176872,
   "Latitude": 106.4612009
 },
 {
   "STT": 168,
   "Name": "Trạm y tế xã Thái Hồng",
   "address": "Xã Thái Hồng, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5142556,
   "Latitude": 106.4965434
 },
 {
   "STT": 169,
   "Name": "Trạm y tế xã Thái An",
   "address": "Xã Thái An, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5170326,
   "Latitude": 106.542203
 },
 {
   "STT": 170,
   "Name": "Trạm y tế xã Thái Phúc",
   "address": "Xã Thái Phúc, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5044156,
   "Latitude": 106.4788714
 },
 {
   "STT": 171,
   "Name": "Trạm y tế xã Thái Hưng",
   "address": "Xã Thái Hưng, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5119634,
   "Latitude": 106.5201084
 },
 {
   "STT": 172,
   "Name": "Trạm y tế xã Thái Đô",
   "address": "Xã Thái Đô, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.4909141,
   "Latitude": 106.5878723
 },
 {
   "STT": 173,
   "Name": "Trạm y tế xã Thái Xuyên",
   "address": "Xã Thái Xuyên, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5038908,
   "Latitude": 106.543676
 },
 {
   "STT": 174,
   "Name": "Trạm y tế xã Thái Hà",
   "address": "Xã Thái Hà, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.5000375,
   "Latitude": 106.4533547
 },
 {
   "STT": 175,
   "Name": "Trạm y tế xã Mỹ Lộc",
   "address": "Xã Mỹ Lộc, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.4828109,
   "Latitude": 106.5525145
 },
 {
   "STT": 176,
   "Name": "Trạm y tế xã Thái Tân",
   "address": "Xã Thái Tân, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.4861198,
   "Latitude": 106.5333649
 },
 {
   "STT": 177,
   "Name": "Trạm y tế xã Thái Thuần",
   "address": "Xã Thái Thuần, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.4847939,
   "Latitude": 106.5024344
 },
 {
   "STT": 178,
   "Name": "Trạm y tế xã Thái Học",
   "address": "Xã Thái Học, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.4901548,
   "Latitude": 106.5215813
 },
 {
   "STT": 179,
   "Name": "Trạm y tế xã Thái Thịnh",
   "address": "Xã Thái Thịnh, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.4778718,
   "Latitude": 106.5142169
 },
 {
   "STT": 180,
   "Name": "Trạm y tế xã Thái Thành",
   "address": "Xã Thái Thành, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.4633974,
   "Latitude": 106.4847619
 },
 {
   "STT": 181,
   "Name": "Trạm y tế xã Thái Thọ",
   "address": "Xã Thái Thọ, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.4541906,
   "Latitude": 106.5201084
 },
 {
   "STT": 182,
   "Name": "Trạm y tế thị trấn Tiền Hải",
   "address": "Thị Trấn Tiền Hải, Huyện Thái Thụy, Tỉnh Thái Bình",
   "Longtitude": 20.402609,
   "Latitude": 106.5009617
 },
 {
   "STT": 183,
   "Name": "Trạm y tế xã Đông Hải",
   "address": "Xã Đông Hải, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.4689609,
   "Latitude": 106.5760857
 },
 {
   "STT": 184,
   "Name": "Trạm y tế xã Đông Trà",
   "address": "Xã Đông Trà, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.45653,
   "Latitude": 106.5554608
 },
 {
   "STT": 185,
   "Name": "Trạm y tế xã Đông Long",
   "address": "Xã Đông Long, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.443559,
   "Latitude": 106.5996595
 },
 {
   "STT": 186,
   "Name": "Trạm y tế xã Đông Quí",
   "address": "Xã Đông Quý, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.4414923,
   "Latitude": 106.5318919
 },
 {
   "STT": 187,
   "Name": "Trạm y tế xã Vũ Lăng",
   "address": "Xã Vũ Lăng, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.4345084,
   "Latitude": 106.4847619
 },
 {
   "STT": 188,
   "Name": "Trạm y tế xã Đông Xuyên",
   "address": "Xã Đông Xuyên, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.4444041,
   "Latitude": 106.5613534
 },
 {
   "STT": 189,
   "Name": "Trạm y tế xã Tây Lương",
   "address": "Xã Tây Lương, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.4327965,
   "Latitude": 106.5024344
 },
 {
   "STT": 190,
   "Name": "Trạm y tế xã Tây Ninh",
   "address": "Xã Tây Ninh, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.4253058,
   "Latitude": 106.5201084
 },
 {
   "STT": 191,
   "Name": "Trạm y tế xã Đông Trung",
   "address": "Xã Đông Trung, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.4282219,
   "Latitude": 106.5495683
 },
 {
   "STT": 192,
   "Name": "Trạm y tế xã Đông Hoàng",
   "address": "Xã Đông Hoàng, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.4227547,
   "Latitude": 106.5760857
 },
 {
   "STT": 193,
   "Name": "Trạm y tế xã Đông Minh",
   "address": "Xã Đông Minh, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.3921574,
   "Latitude": 106.5937658
 },
 {
   "STT": 194,
   "Name": "Trạm y tế xã Tây An",
   "address": "Xã Tây An, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.4166056,
   "Latitude": 106.4906526
 },
 {
   "STT": 195,
   "Name": "Trạm y tế xã Đông Phong",
   "address": "Xã Đông Phong, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.4146398,
   "Latitude": 106.5407299
 },
 {
   "STT": 196,
   "Name": "Trạm y tế xã An Ninh",
   "address": "Xã An Ninh, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.3972392,
   "Latitude": 106.4818167
 },
 {
   "STT": 197,
   "Name": "Trạm y tế xã Tây Sơn",
   "address": "Xã Tây Sơn, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.4027697,
   "Latitude": 106.5142169
 },
 {
   "STT": 198,
   "Name": "Trạm y tế xã Đông Cơ",
   "address": "Xã Đông Cơ, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.3993414,
   "Latitude": 106.5495683
 },
 {
   "STT": 199,
   "Name": "Trạm y tế xã Tây Giang",
   "address": "Xã Tây Giang, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.3917865,
   "Latitude": 106.5083256
 },
 {
   "STT": 200,
   "Name": "Trạm y tế xã Đông Lâm",
   "address": "Xã Đông Lâm, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.3883611,
   "Latitude": 106.543676
 },
 {
   "STT": 201,
   "Name": "Trạm y tế xã Phương Công",
   "address": "Xã Phương Công, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.3819419,
   "Latitude": 106.4906526
 },
 {
   "STT": 202,
   "Name": "Trạm y tế xã Tây Phong",
   "address": "Xã Tây Phong, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.3686796,
   "Latitude": 106.5083256
 },
 {
   "STT": 203,
   "Name": "Trạm y tế xã Tây Tiến",
   "address": "Xã Tây Tiến, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.3721751,
   "Latitude": 106.5318919
 },
 {
   "STT": 204,
   "Name": "Trạm y tế xã Nam Cường",
   "address": "Xã Nam Cường, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.3750943,
   "Latitude": 106.5613534
 },
 {
   "STT": 205,
   "Name": "Trạm y tế xã Vân Trường",
   "address": "Xã Vân Trường, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.3657481,
   "Latitude": 106.4788714
 },
 {
   "STT": 206,
   "Name": "Trạm y tế xã Nam Thắng",
   "address": "Xã Nam Thắng, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.3583394,
   "Latitude": 106.5554608
 },
 {
   "STT": 207,
   "Name": "Trạm y tế Xã Nam Chính",
   "address": "Xã Nam Chính, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.3580201,
   "Latitude": 106.528946
 },
 {
   "STT": 208,
   "Name": "Trạm y tế xã Bắc Hải",
   "address": "Xã Bắc Hải, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.3426395,
   "Latitude": 106.4788714
 },
 {
   "STT": 209,
   "Name": "Trạm y tế xã Nam Thịnh",
   "address": "Xã Nam Thịnh, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.3510946,
   "Latitude": 106.5769834
 },
 {
   "STT": 210,
   "Name": "Trạm y tế xã Nam Hà",
   "address": "Xã Nam Hà, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.3409346,
   "Latitude": 106.4965434
 },
 {
   "STT": 211,
   "Name": "Trạm y tế xã Nam Thanh",
   "address": "Xã Nam Thanh, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.341013,
   "Latitude": 106.5554608
 },
 {
   "STT": 212,
   "Name": "Trạm y tế xã Nam Trung",
   "address": "Xã Nam Trung, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.3337761,
   "Latitude": 106.5407299
 },
 {
   "STT": 213,
   "Name": "Trạm y tế xã Nam Hồng",
   "address": "Xã Nam Hồng, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.3245028,
   "Latitude": 106.5171626
 },
 {
   "STT": 214,
   "Name": "Trạm y tế xã Nam Hưng",
   "address": "Xã Nam Hưng, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.3142249,
   "Latitude": 106.6232359
 },
 {
   "STT": 215,
   "Name": "Trạm y tế xã Nam Hải",
   "address": "Xã Nam Hải, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.3183963,
   "Latitude": 106.4906526
 },
 {
   "STT": 216,
   "Name": "Trạm y tế xã Nam Phú",
   "address": "Xã Nam Phú, Huyện Tiền Hải, Tỉnh Thái Bình",
   "Longtitude": 20.2766678,
   "Latitude": 106.5937658
 },
 {
   "STT": 217,
   "Name": "Trạm y tế thị trấn Thanh Nê",
   "address": "Thị Trấn Thanh Nê, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.3864896,
   "Latitude": 106.4435319
 },
 {
   "STT": 218,
   "Name": "Trạm y tế xã Trà Giang",
   "address": "Xã Trà Giang, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.4856828,
   "Latitude": 106.4635009
 },
 {
   "STT": 219,
   "Name": "Trạm y tế xã Quốc Tuấn",
   "address": "Xã Quốc Tuấn, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.4711323,
   "Latitude": 106.4346979
 },
 {
   "STT": 220,
   "Name": "Trạm y tế xã Vũ Đông",
   "address": "Xã Vũ Đông, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.4703641,
   "Latitude": 106.3787863
 },
 {
   "STT": 221,
   "Name": "Trạm y tế xã An Bình",
   "address": "Xã An Bình, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.4702333,
   "Latitude": 106.4140868
 },
 {
   "STT": 222,
   "Name": "Trạm y tế xã Vũ Tây",
   "address": "Xã Vũ Tây, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.4719374,
   "Latitude": 106.3964218
 },
 {
   "STT": 223,
   "Name": "Trạm y tế xã Hồng Thái",
   "address": "Xã Hồng Thái, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.4573881,
   "Latitude": 106.461512
 },
 {
   "STT": 224,
   "Name": "Trạm y tế xã Bình Nguyên",
   "address": "Xã Bình Nguyên, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.4517587,
   "Latitude": 106.4258643
 },
 {
   "STT": 225,
   "Name": "Trạm y tế xã Vũ Sơn",
   "address": "Xã Vũ Sơn, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.4551651,
   "Latitude": 106.3905338
 },
 {
   "STT": 226,
   "Name": "Trạm y tế xã Lê Lợi",
   "address": "Xã Lê Lợi, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.4406782,
   "Latitude": 106.4596261
 },
 {
   "STT": 227,
   "Name": "Trạm y tế xã Quyết Tiến",
   "address": "Xã Quyết Tiến, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.4456034,
   "Latitude": 106.4454446
 },
 {
   "STT": 228,
   "Name": "Trạm y tế xã Vũ Lạc",
   "address": "Xã Vũ Lạc, TP Thái Bình",
   "Longtitude": 20.4341018,
   "Latitude": 106.3802702
 },
 {
   "STT": 229,
   "Name": "Trạm y tế xã Vũ Lễ",
   "address": "Xã Vũ Lễ, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.4373378,
   "Latitude": 106.4019713
 },
 {
   "STT": 230,
   "Name": "Trạm y tế xã Thanh Tân",
   "address": "Xã Thanh Tân, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.4344216,
   "Latitude": 106.4258643
 },
 {
   "STT": 231,
   "Name": "Trạm y tế xã Thượng Hiền",
   "address": "Xã Thượng Hiền, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.4204711,
   "Latitude": 106.4656184
 },
 {
   "STT": 232,
   "Name": "Trạm y tế xã Nam Cao",
   "address": "Xã Nam Cao, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.4258006,
   "Latitude": 106.4553111
 },
 {
   "STT": 233,
   "Name": "Trạm y tế xã Đình Phùng",
   "address": "Xã Đình Phùng, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.4153811,
   "Latitude": 106.4435319
 },
 {
   "STT": 234,
   "Name": "Trạm y tế xã Vũ Ninh",
   "address": "Xã Vũ Ninh, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.4141456,
   "Latitude": 106.3771285
 },
 {
   "STT": 235,
   "Name": "Trạm y tế xã Vũ An",
   "address": "Xã Vũ An, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.4204865,
   "Latitude": 106.3905338
 },
 {
   "STT": 236,
   "Name": "Trạm y tế xã Quang Lịch",
   "address": "Xã Quang Lịch, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.4187867,
   "Latitude": 106.4081983
 },
 {
   "STT": 237,
   "Name": "Trạm y tế xã Hòa Bình",
   "address": "Xã Hòa Bình, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.4060945,
   "Latitude": 106.4199755
 },
 {
   "STT": 238,
   "Name": "Trạm y tế xã Bình Minh",
   "address": "Xã Bình Minh, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.3986138,
   "Latitude": 106.4376425
 },
 {
   "STT": 239,
   "Name": "Trạm y tế xã Vũ Quí",
   "address": "Xã Vũ Quý, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.4037136,
   "Latitude": 106.3846459
 },
 {
   "STT": 240,
   "Name": "Trạm y tế xã Quang Bình",
   "address": "Xã Quang Bình, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.3936299,
   "Latitude": 106.3993658
 },
 {
   "STT": 241,
   "Name": "Trạm y tế xã An Bồi",
   "address": "Xã An Bồi, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.3899952,
   "Latitude": 106.4670909
 },
 {
   "STT": 242,
   "Name": "Trạm y tế xã Vũ Trung",
   "address": "Xã Vũ Trung, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.3765102,
   "Latitude": 106.3669834
 },
 {
   "STT": 243,
   "Name": "Trạm y tế xã Vũ Thắng",
   "address": "Xã Vũ Thắng, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.3765102,
   "Latitude": 106.3669834
 },
 {
   "STT": 244,
   "Name": "Trạm y tế xã Vũ Công",
   "address": "Xã Vũ Công, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.3731201,
   "Latitude": 106.40231
 },
 {
   "STT": 245,
   "Name": "Trạm y tế xã Vũ Hòa",
   "address": "Xã Vũ Hòa, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.3690368,
   "Latitude": 106.3846459
 },
 {
   "STT": 246,
   "Name": "Trạm y tế xã Quang Minh",
   "address": "Xã Quang Minh, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.3662097,
   "Latitude": 106.4140868
 },
 {
   "STT": 247,
   "Name": "Trạm y tế xã Quang Trung",
   "address": "Xã Quang Trung, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.3680204,
   "Latitude": 106.4553111
 },
 {
   "STT": 248,
   "Name": "Trạm y tế xã Minh Hưng",
   "address": "Xã Minh Hưng, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.3535206,
   "Latitude": 106.4258643
 },
 {
   "STT": 249,
   "Name": "Trạm y tế xã Quang Hưng",
   "address": "Xã Quang Hưng, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.3544267,
   "Latitude": 106.4464766
 },
 {
   "STT": 250,
   "Name": "Trạm y tế xã Vũ Bình",
   "address": "Xã Vũ Bình, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.3511344,
   "Latitude": 106.3905338
 },
 {
   "STT": 251,
   "Name": "Trạm y tế xã Minh Tân",
   "address": "Xã Minh Tân, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.3373169,
   "Latitude": 106.4140868
 },
 {
   "STT": 252,
   "Name": "Trạm y tế xã Nam Bình",
   "address": "Xã Nam Bình, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.3339212,
   "Latitude": 106.4494214
 },
 {
   "STT": 253,
   "Name": "Trạm y tế xã Bình Thanh",
   "address": "Xã Bình Thanh, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.3182861,
   "Latitude": 106.4317533
 },
 {
   "STT": 254,
   "Name": "Trạm y tế xã Bình Định",
   "address": "Xã Bình Định, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.3186276,
   "Latitude": 106.458256
 },
 {
   "STT": 255,
   "Name": "Trạm y tế Xã Hồng Tiến",
   "address": "Xã Hồng Tiến, Huyện Kiến Xương, Tỉnh Thái Bình",
   "Longtitude": 20.2975578,
   "Latitude": 106.4670909
 },
 {
   "STT": 256,
   "Name": "Trạm y tế thị trấn Vũ Thư",
   "address": "Thị Trấn Vũ Thư, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.4359144,
   "Latitude": 106.2823717
 },
 {
   "STT": 257,
   "Name": "Trạm y tế xã Hồng Lý",
   "address": "Xã Hồng Lý, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.4980171,
   "Latitude": 106.1845606
 },
 {
   "STT": 258,
   "Name": "Trạm y tế xã Đồng Thanh",
   "address": "Xã Đồng Thanh, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.5079049,
   "Latitude": 106.2022072
 },
 {
   "STT": 259,
   "Name": "Trạm y tế xã Xuân Hòa",
   "address": "Xã Xuân Hòa, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.5030486,
   "Latitude": 106.2227968
 },
 {
   "STT": 260,
   "Name": "Trạm y tế xã Hiệp Hòa",
   "address": "Xã Hiệp Hòa, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.4976277,
   "Latitude": 106.2492724
 },
 {
   "STT": 261,
   "Name": "Trạm y tế xã Phúc Thành",
   "address": "Xã Phúc Thành, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.4983386,
   "Latitude": 106.3022339
 },
 {
   "STT": 262,
   "Name": "Trạm y tế xã Tân Phong",
   "address": "Xã Tân Phong, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.4908622,
   "Latitude": 106.3198908
 },
 {
   "STT": 263,
   "Name": "Trạm y tế xã Song Lãng",
   "address": "Xã Song Lãng, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.4849364,
   "Latitude": 106.2610404
 },
 {
   "STT": 264,
   "Name": "Trạm y tế xã Tân Hòa",
   "address": "Xã Tân Hòa, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.4846355,
   "Latitude": 106.3001572
 },
 {
   "STT": 265,
   "Name": "Trạm y tế xã Việt Hùng",
   "address": "Xã Việt Hùng, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.4672242,
   "Latitude": 106.2345633
 },
 {
   "STT": 266,
   "Name": "Trạm y tế xã Minh Lãng",
   "address": "Xã Minh Lãng, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.4742921,
   "Latitude": 106.2816361
 },
 {
   "STT": 267,
   "Name": "Trạm y tế xã Tân Bình",
   "address": "Xã Tân Bình, TP Thái Bình",
   "Longtitude": 20.4693246,
   "Latitude": 106.322603
 },
 {
   "STT": 268,
   "Name": "Trạm y tế xã Minh Khai",
   "address": "Xã Minh Khai, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.4554603,
   "Latitude": 106.2669247
 },
 {
   "STT": 269,
   "Name": "Trạm y tế xã Dũng Nghĩa",
   "address": "Xã Dũng Nghĩa, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.4467023,
   "Latitude": 106.237505
 },
 {
   "STT": 270,
   "Name": "Trạm y tế xã Minh Quang",
   "address": "Xã Minh Quang, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.4479904,
   "Latitude": 106.2845785
 },
 {
   "STT": 271,
   "Name": "Trạm y tế xã Tam Quang",
   "address": "Xã Tam Quang, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.4455801,
   "Latitude": 106.2492724
 },
 {
   "STT": 272,
   "Name": "Trạm y tế xã Tân Lập",
   "address": "Xã Tân Lập, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.4335502,
   "Latitude": 106.2389759
 },
 {
   "STT": 273,
   "Name": "Trạm y tế xã Bách Thuận",
   "address": "Xã Bách Thuận, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.4220787,
   "Latitude": 106.2227968
 },
 {
   "STT": 274,
   "Name": "Trạm y tế xã Tự Tân",
   "address": "Xã Tự Tân, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.421327,
   "Latitude": 106.2610404
 },
 {
   "STT": 275,
   "Name": "Trạm y tế xã Song An",
   "address": "Xã Song An, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.4179558,
   "Latitude": 106.2963486
 },
 {
   "STT": 276,
   "Name": "Trạm y tế Xã Trung An",
   "address": "Xã Trung An, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.4052676,
   "Latitude": 106.3081194
 },
 {
   "STT": 277,
   "Name": "Trạm y tế xã Vũ Hội",
   "address": "Xã Vũ Hội, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.4123196,
   "Latitude": 106.3552092
 },
 {
   "STT": 278,
   "Name": "Trạm y tế xã Hòa Bình",
   "address": "Xã Hòa Bình, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.4196424,
   "Latitude": 106.2786937
 },
 {
   "STT": 279,
   "Name": "Trạm y tế xã Nguyên Xá",
   "address": "Xã Nguyên Xá, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.4011731,
   "Latitude": 106.2904635
 },
 {
   "STT": 280,
   "Name": "Trạm y tế xã Việt Thuận",
   "address": "Xã Việt Thuận, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.3914536,
   "Latitude": 106.3316629
 },
 {
   "STT": 281,
   "Name": "Trạm y tế xã Vũ Vinh",
   "address": "Xã Vũ Vinh, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.3928559,
   "Latitude": 106.3535803
 },
 {
   "STT": 282,
   "Name": "Trạm y tế xã Vũ Đoài",
   "address": "Xã Vũ Đoài, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.3784089,
   "Latitude": 106.3169479
 },
 {
   "STT": 283,
   "Name": "Trạm y tế xã Vũ Tiến",
   "address": "Xã Vũ Tiến, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.3690942,
   "Latitude": 106.293406
 },
 {
   "STT": 284,
   "Name": "Trạm y tế xã Vũ Vân",
   "address": "Xã Vũ Vân, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.3597349,
   "Latitude": 106.3610962
 },
 {
   "STT": 285,
   "Name": "Trạm y tế xã Duy Nhất",
   "address": "Xã Duy Nhất, Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.3554822,
   "Latitude": 106.2845785
 }
];
