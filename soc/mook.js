var mook = {
    sobn: function () {
        let raw = `
        Bệnh viện đa khoa huyện Kiến Xương
        Bệnh viện đa khoa huyện Tiền Hải
        Bệnh viện huyện Vinh Thuận
        Bệnh viện đa khoa huyện Quỳnh Phụ
        Bệnh viện đa khoa huyện Hưng Hà
        Bệnh viện đa khoa huyện Thái Thụy
        Bệnh viện Da liễu cơ sở 2`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    tinhuy: function () {
        let raw = `
        Bệnh viện đa khoa huyện Kiến Xương
        Bệnh viện đa khoa huyện Tiền Hải
        Bệnh viện huyện Vinh Thuận
        Bệnh viện đa khoa huyện Quỳnh Phụ
        Bệnh viện đa khoa huyện Hưng Hà
        Bệnh viện đa khoa huyện Thái Thụy
        Bệnh viện Da liễu cơ sở 2`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    ubnd: function () {
        let raw = `TP. Thái Bình
                H. An Biên
                H. Châu Thành
                H. Giang Thành‎
                H. Giồng Riềng‎ 
                H. Gò Quao‎ 
                TP. Hà Tiên‎ 
                H. Hòn Đất
                H. Phú Quốc‎`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },
    cdv: function () {
        let raw = `
        Bệnh viện đa khoa huyện Kiến Xương
        Bệnh viện đa khoa huyện Tiền Hải
        Bệnh viện huyện Vinh Thuận
        Bệnh viện đa khoa huyện Quỳnh Phụ
        Bệnh viện đa khoa huyện Hưng Hà
        Bệnh viện đa khoa huyện Thái Thụy
        Bệnh viện Da liễu cơ sở 2`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    http: function () {
        let raw = `
        Bệnh viện đa khoa huyện Kiến Xương
        Bệnh viện đa khoa huyện Tiền Hải
        Bệnh viện huyện Vinh Thuận
        Bệnh viện đa khoa huyện Quỳnh Phụ
        Bệnh viện đa khoa huyện Hưng Hà
        Bệnh viện đa khoa huyện Thái Thụy
        Bệnh viện Da liễu cơ sở 2`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    }
}
