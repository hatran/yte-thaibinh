var data = [
 {
   "STT": 1,
   "Name": "Bệnh viện Đại học Y Thái Bình Sài Gòn",
   "address": "Trịnh Văn Cấn, Cầu Ông Lãnh, Phường Cầu Ông Lãnh, Quận 1, Thành phố Hồ Chí Minh, Việt Nam",
   "lat": 10.76734,
   "lon": 106.69677,
   "Number_of_beds": "Đặng Thị Minh",
    "area": "02033.819.115",
    "laixe": "Bùi Đức Toàn",
    "bacsi": "Nông Văn Dũng",
    "dieuduong": "Phạm Thái Dương"
 },
 {
   "STT": 2,
   "Name": "Bệnh viện Đa Khoa Sài Gòn",
   "address": "Huỳnh Thúc Kháng, Phường Bến Thành, Quận 1, Thành phố Hồ Chí Minh, Việt Nam",
   "lat": 10.77200,
   "lon": 106.69954,
   "Number_of_beds": " Nguyễn Đức Bằng",
    "area": "02033.670.297",
    "laixe": "Nguyễn Văn Sinh",
    "bacsi": "Nguyễn Văn Hùng",
    "dieuduong": "Phạm Thế Ninh"
 },
 {
   "STT": 3,
   "Name": "Bệnh viện Quận 4",
   "address": "Bến Vân Đồn, Phường 9, Quận 4, Thành phố Hồ Chí Minh, Việt Nam",
   "lat": 10.76531,
   "lon": 106.70218,
   "Number_of_beds": "Vũ Duy Điều",
    "area": "0359.008.555",
    "laixe": "Ngô Quang Huynh",
    "bacsi": "Nguyễn Đức Cử",
    "dieuduong": "Nguyễn Thị Khánh"
 }
];