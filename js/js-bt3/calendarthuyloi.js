angular.module('calendarDemoApp', ['ui.rCalendar']);

angular.module('calendarDemoApp').controller('CalendarDemoCtrl', ['$scope', function($scope) {
    'use strict';
    $scope.changeMode = function(mode) {
        $scope.mode = mode;
    };

    $scope.today = function() {
        $scope.currentDate = new Date();
    };

    $scope.isToday = function() {
        var today = new Date(),
            currentCalendarDate = new Date($scope.currentDate);

        today.setHours(0, 0, 0, 0);
        currentCalendarDate.setHours(0, 0, 0, 0);
        return today.getTime() === currentCalendarDate.getTime();
    };

    $scope.loadEvents = function() {
        var date = new Date();
        $scope.eventSource = [{
                title: "Hội nghị về công tác quản lý nước sạch",
                noidung: "Hội nghị về công tác quản lý nước sạch",
                thamdu: "Theo giấy mời",
                diadiem: "Hà Nội",
                startTime: new Date(2018, 8, 21, 8, 0, 0, 0),
                endTime: new Date(2018, 8, 21, 23, 59, 59, 0),
                allDay: false
            },
            {
                title: "Hội nghị phổ biến hệ thống luật pháp về thủy lợi",
                noidung: "Hội nghị phổ biến hệ thống luật pháp về thủy lợi",
                thamdu: "VPTC*, Vụ PCTTr, Cục QLCT, Vụ ATĐ ",
                diadiem: "Hà Nội",
                startTime: new Date(2018, 8, 21, 14, 0, 0, 0),
                endTime: new Date(2018, 8, 21, 16, 0, 0, 0),
                allDay: false
            },
            {
                title: "Chuyến bay HN - Tỉnh Thái Bình",
                noidung: "Chuyến bay HN - Tỉnh Thái Bình",
                thamdu: "",
                diadiem: "",
                startTime: new Date(2018, 8, 21, 8, 0, 0, 0),
                endTime: new Date(2018, 8, 21, 13, 0, 0, 0),
                allDay: false
            }
        ];

        console.log($scope.eventSource);
    };

    $scope.onEventSelected = function(event) {
        console.log(event);
        $scope.event = event;
    };

    $scope.onTimeSelected = function(selectedTime, events) {

        console.log(events);
        //console.log('Selected time: ' + selectedTime + ' hasEvents: ' + (events !== undefined && events.length !== 0));
    };

    $scope.onclick = function(event) {
        console.log(event);
    }

    function createRandomEvents() {
        var events = [];
        for (var i = 0; i < 50; i += 1) {
            var date = new Date();
            var eventType = Math.floor(Math.random() * 2);
            var startDay = Math.floor(Math.random() * 90) - 45;
            var endDay = Math.floor(Math.random() * 2) + startDay;
            var startTime;
            var endTime;
            if (eventType === 0) {
                startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));
                if (endDay === startDay) {
                    endDay += 1;
                }
                endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
                events.push({
                    title: 'All Day - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: true
                });
            } else {
                var startMinute = Math.floor(Math.random() * 24 * 60);
                var endMinute = Math.floor(Math.random() * 180) + startMinute;
                startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
                endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
                events.push({
                    title: 'Event - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: false
                });
            }

            //console.log(startTime);
            //console.log(endTime);
        }
        return events;
    }
}]);

// end bộ trưởng  ----->

angular.module('calendarDemoApp').controller('CalendarDemoCtrl_ttr1', ['$scope', function($scope) {
    'use strict';
    $scope.changeMode = function(mode) {
        $scope.mode = mode;
    };

    $scope.today = function() {
        $scope.currentDate = new Date();
    };

    $scope.isToday = function() {
        var today = new Date(),
            currentCalendarDate = new Date($scope.currentDate);

        today.setHours(0, 0, 0, 0);
        currentCalendarDate.setHours(0, 0, 0, 0);
        return today.getTime() === currentCalendarDate.getTime();
    };

    $scope.loadEvents = function() {
        var date = new Date();
        $scope.eventSource = [{
                title: "Hội nghị về công tác quản lý nước sạch",
                noidung: "Hội nghị về công tác quản lý nước sạch",
                thamdu: "Theo giấy mời",
                diadiem: "Hà Nội",
                startTime: new Date(2018, 8, 21, 8, 30, 0, 0),
                endTime: new Date(2018, 8, 21, 9, 30, 0, 0),
                allDay: false
            },
            {
                title: " Hội nghị phổ biến pháp luật về thủy lợi",
                noidung: "Hội nghị phổ biến pháp luật về thủy lợi",
                thamdu: "Theo giấy mời",
                diadiem: "Hà Nội",
                startTime: new Date(2018, 8, 21, 8, 30, 0, 0),
                endTime: new Date(2018, 8, 21, 11, 0, 0, 0),
                allDay: false
            }
        ];

        console.log($scope.eventSource);
    };

    $scope.onEventSelected = function(event) {
        console.log(event);
        $scope.event = event;
    };

    $scope.onTimeSelected = function(selectedTime, events) {

        console.log(events);
        //console.log('Selected time: ' + selectedTime + ' hasEvents: ' + (events !== undefined && events.length !== 0));
    };

    $scope.onclick = function(event) {
        console.log(event);
    }

    function createRandomEvents() {
        var events = [];
        for (var i = 0; i < 50; i += 1) {
            var date = new Date();
            var eventType = Math.floor(Math.random() * 2);
            var startDay = Math.floor(Math.random() * 90) - 45;
            var endDay = Math.floor(Math.random() * 2) + startDay;
            var startTime;
            var endTime;
            if (eventType === 0) {
                startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));
                if (endDay === startDay) {
                    endDay += 1;
                }
                endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
                events.push({
                    title: 'All Day - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: true
                });
            } else {
                var startMinute = Math.floor(Math.random() * 24 * 60);
                var endMinute = Math.floor(Math.random() * 180) + startMinute;
                startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
                endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
                events.push({
                    title: 'Event - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: false
                });
            }

            //console.log(startTime);
            //console.log(endTime);
        }
        return events;
    }
}]);

// end thứ trưởng 1  ----->
angular.module('calendarDemoApp').controller('CalendarDemoCtrl_ttr2', ['$scope', function($scope) {
    'use strict';
    $scope.changeMode = function(mode) {
        $scope.mode = mode;
    };

    $scope.today = function() {
        $scope.currentDate = new Date();
    };

    $scope.isToday = function() {
        var today = new Date(),
            currentCalendarDate = new Date($scope.currentDate);

        today.setHours(0, 0, 0, 0);
        currentCalendarDate.setHours(0, 0, 0, 0);
        return today.getTime() === currentCalendarDate.getTime();
    };

    $scope.loadEvents = function() {
        var date = new Date();
        $scope.eventSource = [{
                title: "Nghe báo cáo kết quả triển khai tiêu chuẩn quốc gia",
                noidung: "Nghe báo cáo kết quả triển khai tiêu chuẩn quốc gia",
                thamdu: "Vụ KHCN và HTQT*",
                diadiem: "Phòng họp T2-A6B",
                startTime: new Date(2018, 8, 21, 8, 0, 0, 0),
                endTime: new Date(2018, 8, 21, 23, 59, 0, 0),
                allDay: false
            },
            {
                title: "Nghe báo cáo Demo giới thiệu trung tâm điều hành thông minh (Lãnh đạo Vụ KHCN&HTQT thay mặt tham dự)",
                noidung: "Nghe báo cáo Demo giới thiệu trung tâm điều hành thông minh (Lãnh đạo Vụ KHCN&HTQT thay mặt tham dự)",
                thamdu: "Theo giấy mời",
                diadiem: "Tầng 19, tòa nhà Lotte 54 Liễu Giai",
                startTime: new Date(2018, 8, 21, 8, 0, 0, 0),
                endTime: new Date(2018, 8, 21, 8, 30, 0, 0),
                allDay: false
            }
        ];

        console.log($scope.eventSource);
    };

    $scope.onEventSelected = function(event) {
        console.log(event);
        $scope.event = event;
    };

    $scope.onTimeSelected = function(selectedTime, events) {

        console.log(events);
        //console.log('Selected time: ' + selectedTime + ' hasEvents: ' + (events !== undefined && events.length !== 0));
    };

    $scope.onclick = function(event) {
        console.log(event);
    }

    function createRandomEvents() {
        var events = [];
        for (var i = 0; i < 50; i += 1) {
            var date = new Date();
            var eventType = Math.floor(Math.random() * 2);
            var startDay = Math.floor(Math.random() * 90) - 45;
            var endDay = Math.floor(Math.random() * 2) + startDay;
            var startTime;
            var endTime;
            if (eventType === 0) {
                startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));
                if (endDay === startDay) {
                    endDay += 1;
                }
                endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
                events.push({
                    title: 'All Day - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: true
                });
            } else {
                var startMinute = Math.floor(Math.random() * 24 * 60);
                var endMinute = Math.floor(Math.random() * 180) + startMinute;
                startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
                endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
                events.push({
                    title: 'Event - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: false
                });
            }

            //console.log(startTime);
            //console.log(endTime);
        }
        return events;
    }
}]);

