var data2 = [
 {
   "Name": "Nhà thuốc doanh nghiệp số 12",
   "address": "359 Lý Bôn, P. Kỳ Bá",
   "Longtitude": 20.442682,
   "Latitude": 106.3387498
 },
 {
   "Name": "Công ty Dược Phẩm Đức Hưng",
   "address": "508 Lý Bôn, Thái Bình",
   "Longtitude": 20.4425903,
   "Latitude": 106.3385173
 },
 {
   "Name": "Nhà thuốc Thái Bình Dương",
   "address": "734 Lý Bôn",
   "Longtitude": 20.4368978,
   "Latitude": 106.3416592
 },
 {
   "Name": "Nhà thuốc doanh nghiệp số 9 Ds Ngô Hồng Thới",
   "address": "357 Lý Bôn, P. Đề Thám",
   "Longtitude": 20.442787,
   "Latitude": 106.3385743
 },
 {
   "Name": "Nhà thuốc 858 Ds Bình",
   "address": "858, Đường Lý Bôn, P. Trần Lãm",
   "Longtitude": 20.4361618,
   "Latitude": 106.3420278
 },
 {
   "Name": "Nhà thuốc Số 28",
   "address": "28 Trần Phú, P. Trần Hưng Đạo",
   "Longtitude": 10.7547877,
   "Latitude": 106.6789365
 },
 {
   "Name": "Nhà thuốc Thu Hiền",
   "address": "327, Tổ 30, Đường Trần Hưng Đạo",
   "Longtitude": 10.7673224,
   "Latitude": 106.6944179
 },
 {
   "Name": "Nhà thuốc Hương Lan",
   "address": "465 Lý Thường Kiệt",
   "Longtitude": 10.778393,
   "Latitude": 106.652649
 },
 {
   "Name": "Nhà thuốc doanh nghiệp Số 6 Ds Nguyễn Thị Hoà",
   "address": "122 Lý Thường Kiệt, P. Lê Hồng Phong",
   "Longtitude": 20.5449658,
   "Latitude": 105.9006344
 },
 {
   "Name": "Công ty cổ phần Dược Ánh Dương Quầy Số 01",
   "address": "252 Lý Thường Kiệt",
   "Longtitude": 10.7667485,
   "Latitude": 106.6593997
 },
 {
   "Name": "Nhà thuốc Khoa Bình",
   "address": "166 Trần Thánh Tông",
   "Longtitude": 20.4520257,
   "Latitude": 106.3365134
 },
 {
   "Name": "Nhà thuốc Hưng Thịnh",
   "address": "46 Minh Khai, P. Bồ Xuyên",
   "Longtitude": 20.4501902,
   "Latitude": 106.3379448
 },
 {
   "Name": "Nhà thuốc Mai Anh",
   "address": "32 Minh Khai, P. Bồ Xuyên",
   "Longtitude": 20.4514386,
   "Latitude": 106.3399289
 },
 {
   "Name": "Nhà thuốc doanh nghiệp số 02 Ds Bùi Thị Hương",
   "address": "64 Hai Bà Trưng, P. Lê Hồng Phong",
   "Longtitude": 20.449085,
   "Latitude": 106.3448981
 },
 {
   "Name": "Công ty Dược Phẩm Ánh Dương",
   "address": "07 Võ Nguyên Giáp, P. Hoàng Diệu",
   "Longtitude": 20.4592118,
   "Latitude": 106.3548612
 },
 {
   "Name": "Nhà thuốc Cao Phong",
   "address": "113, Lý Thái Tổ, Tổ 15, P. Kỳ Bá",
   "Longtitude": 20.4486136,
   "Latitude": 106.3471005
 },
 {
   "Name": "Nhà thuốc Tâm",
   "address": "38 Ngô Thì Nhậm",
   "Longtitude": 21.0174243,
   "Latitude": 105.8532393
 },
 {
   "Name": "Công ty Dược Phẩm Bích Huyền Nhà thuốc Tuyết Hiệp",
   "address": "90 Trần Thái Tông, P. Bồ Xuyên",
   "Longtitude": 20.4529164,
   "Latitude": 106.3389384
 },
 {
   "Name": "Nhà thuốc doanh nghiệp số 16",
   "address": "20 Đường Phan Bội Châu, P. Lê Hồng Phong",
   "Longtitude": 20.4486673,
   "Latitude": 106.3444966
 },
 {
   "Name": "Nhà thuốc Hà Ds Trịnh Thị Hà",
   "address": "42 Phan Bội Châu, P. Lê Hồng Phong",
   "Longtitude": 20.4487159,
   "Latitude": 106.3443195
 },
 {
   "Name": "Nhà thuốc doanh nghiệp số 15",
   "address": "20 Phan Bội Châu, P Lê Hồng Phong",
   "Longtitude": 20.4492353,
   "Latitude": 106.3438972
 },
 {
   "Name": "Công ty dược Tiến Thành",
   "address": "476 đường Long Hưng, P. Hoàng Diệu",
   "Longtitude": 20.4701824,
   "Latitude": 106.3559433
 },
 {
   "Name": "Nhà thuốc Vân Trường",
   "address": "Xóm 11, Thôn Quyến, Xã Vũ Chính",
   "Longtitude": 20.4278977,
   "Latitude": 106.3554272
 },
 {
   "Name": "Nhà thuốc Mai Trường",
   "address": "239, Đường Lê Đại Hành, P. Kỳ Bá",
   "Longtitude": 10.764021,
   "Latitude": 106.6554759
 },
 {
   "Name": "Nhà thuốc Hiền Len Ds Bùi Thị Vinh",
   "address": "235 Lê Đại Hành, Kỳ Bá",
   "Longtitude": 20.4455371,
   "Latitude": 106.3447714
 },
 {
   "Name": "Công ty cổ phần Dược Phẩm Sao Mai Ds Đào Trọng Tài",
   "address": "Km4 Đường Hùng Vương",
   "Longtitude": 20.436318,
   "Latitude": 106.2971975
 },
 {
   "Name": "Nhà thuốc Phạm Gia",
   "address": "535 đường Trần Lãm, P. Trần Lãm",
   "Longtitude": 20.4374454,
   "Latitude": 106.3515149
 },
 {
   "Name": "Nhà thuốc Khánh Hưng(Key)",
   "address": "74 Đường Bồ Xuyên",
   "Longtitude": 20.4555003,
   "Latitude": 106.3419641
 },
 {
   "Name": "Nhà thuốc 24 Chu Văn An",
   "address": "Số 24 Đường Chu Văn An,P. Quang Trung",
   "Longtitude": 10.747294,
   "Latitude": 106.652376
 },
 {
   "Name": "Nhà thuốc Thanh Huyền",
   "address": "Chợ Tông, Xã Vũ Chính",
   "Longtitude": 20.4240117,
   "Latitude": 106.3453673
 },
 {
   "Name": "Nhà thuốc Đức Anh",
   "address": "Lô 7.3, Khu Sông Trà, Lý Bôn",
   "Longtitude": 20.4762734,
   "Latitude": 106.2794445
 },
 {
   "Name": "QT Huy Nhị",
   "address": "Số 254, Nguyễn Du, TT Quỳnh Côi, huyện Quỳnh Phụ, Thái Bình",
   "Longtitude": 20.6454735,
   "Latitude": 106.3293834
 },
 {
   "Name": "QT BV Quỳnh Côi Ds Nguyễn Thị Hà",
   "address": "Khu 3 TT Quỳnh Côi",
   "Longtitude": 20.6537848,
   "Latitude": 106.3243053
 },
 {
   "Name": "Quầy thuốc Hướng Dương Ds Mai Xuân Dương",
   "address": "Số 120, Trần Hưng Đạo, TT An Bài, huyện Quỳnh Phụ, Thái Bình",
   "Longtitude": 20.6482726,
   "Latitude": 106.4258643
 },
 {
   "Name": "Quầy thuốc Hương Vị",
   "address": "Số 98, Trần Hưng Đạo, TT An Bài",
   "Longtitude": 15.8802038,
   "Latitude": 108.3243219
 },
 {
   "Name": "Quầy thuốc Tân Dược Đức Linh",
   "address": "09, Trần Hưng Đạo, TT Quỳnh Côi",
   "Longtitude": 20.6607447,
   "Latitude": 106.3274188
 },
 {
   "Name": "Nhà thuốc Văn Trọng",
   "address": "211, tổ 12, phố Cầu Tây, TT Quỳnh Côi",
   "Longtitude": 20.6527858,
   "Latitude": 106.3229592
 },
 {
   "Name": "Đại lý thuốc Tân Dược chị Phương",
   "address": "Chợ Quỳnh Trang, Xã Quỳnh Trang",
   "Longtitude": 19.2589692,
   "Latitude": 105.6588485
 },
 {
   "Name": "Quầy thuốc Cường Hương",
   "address": "Thôn Ký An, Xã Quỳnh Minh",
   "Longtitude": 20.6693737,
   "Latitude": 106.3619863
 },
 {
   "Name": "Đại lý thuốc Tân Dược Ds Nguyễn Thị Quế",
   "address": "Kiot 4 Chợ TT Hưng Hà, Khu Nhân Cầu 2, TT Hưng Hà",
   "Longtitude": 20.5916721,
   "Latitude": 106.2249955
 },
 {
   "Name": "Đại lý thuốc Tân Dược Ds Phạm Văn Cường",
   "address": "Ngã Tư La, Xã Minh Khai",
   "Longtitude": 20.5719545,
   "Latitude": 106.2390791
 },
 {
   "Name": "Nhà thuốc Nguyễn Thanh Phòng",
   "address": "Ngã Tư La, Xã Minh Khai",
   "Longtitude": 20.5719545,
   "Latitude": 106.2390791
 },
 {
   "Name": "Quầy thuốc Quốc Hưng Ds Hưng",
   "address": "Ngã Tư La, Xã Minh Khai",
   "Longtitude": 20.5719545,
   "Latitude": 106.2390791
 },
 {
   "Name": "Đại lý Sao Mai Ds Hoàng Thị Hà",
   "address": "Kiot 9 Chợ Khánh Mỹ, Phúc Khánh",
   "Longtitude": 20.6053895,
   "Latitude": 106.1865825
 },
 {
   "Name": "Đại lý Sao Mai Quầy thuốc Bính Nghi",
   "address": "Thi An TT Hưng Nhân",
   "Longtitude": 20.6398629,
   "Latitude": 106.1455966
 },
 {
   "Name": "Quầy thuốc Trọng Thùy",
   "address": "Ngã tư đường mới Tịnh Xuyên, Xã Hồng Minh",
   "Longtitude": 20.5252582,
   "Latitude": 106.2022072
 },
 {
   "Name": "Đại lý thuốc Tân Dược Ds Đỗ Thị Thắm",
   "address": "Ngã Tư Việt Yên, Xã Điệp Nông",
   "Longtitude": 20.6549225,
   "Latitude": 106.237505
 },
 {
   "Name": "Quầy thuốc Đôn Lan",
   "address": "Cổng chợ Đô Kỳ, Xã Đông Đô",
   "Longtitude": 20.6074828,
   "Latitude": 106.2834751
 },
 {
   "Name": "Quầy thuốc Phương Dung",
   "address": "Chợ Diêm",
   "Longtitude": 21.0707097,
   "Latitude": 105.9044971
 },
 {
   "Name": "Quầy Thuốc Ngọc Thúy",
   "address": "Chợ Buộm, Tân Tiến, Hưng Hà",
   "Longtitude": 20.6228845,
   "Latitude": 106.2201595
 },
 {
   "Name": "Quầy thuốc Thu Hiền",
   "address": "Thôn Cổ Trai, xã Hồng Minh, huyện Hưng Hà",
   "Longtitude": 20.5252582,
   "Latitude": 106.2022072
 },
 {
   "Name": "Nhà thuốc Trường Giang",
   "address": "Tổ 8 TT Đông Hưng",
   "Longtitude": 20.5571341,
   "Latitude": 106.3526066
 },
 {
   "Name": "Nhà thuốc Trinh Đức",
   "address": "Số 17, Nguyễn Văn Năng, TT Đông Hưng",
   "Longtitude": 20.5566118,
   "Latitude": 106.3540639
 },
 {
   "Name": "Nhà thuốc Thuý Ninh",
   "address": "Tổ 03 TT Đông Hưng",
   "Longtitude": 20.5577783,
   "Latitude": 106.3549942
 },
 {
   "Name": "Quầy thuốc Nam Phong",
   "address": "Ngã Tư Vô Hối, Xã Đông Kinh",
   "Longtitude": 20.5423751,
   "Latitude": 106.4399213
 },
 {
   "Name": "Quầy thuốc Khang Nhài",
   "address": "Tiên Hưng, Xã Thăng Long",
   "Longtitude": 20.5480926,
   "Latitude": 106.2821233
 },
 {
   "Name": "Quầy thuốc Hòa Phượng",
   "address": "Phố Thăng Long , TT Tiên Hưng, Xã Thăng Long",
   "Longtitude": 20.5461438,
   "Latitude": 106.2840715
 },
 {
   "Name": "Hiệu thuốc Phơn Huế",
   "address": "Phố Tiên Hưng, Xã Thăng Long",
   "Longtitude": 20.5480926,
   "Latitude": 106.2821233
 },
 {
   "Name": "Quầy thuốc Thu Thu",
   "address": "Xóm 2, Xã Chương Dương",
   "Longtitude": 20.845772,
   "Latitude": 105.9049796
 },
 {
   "Name": "QT trung tâm Dược Đông Hưng Ds Đỗ Thị Hiền",
   "address": "Khu 2 TT Đông Hưng",
   "Longtitude": 20.556074,
   "Latitude": 106.353684
 },
 {
   "Name": "Trung tâm dược Đông Hưng Ds Lê Văn Lương",
   "address": "Khu 2 TT Đông Hưng",
   "Longtitude": 20.556074,
   "Latitude": 106.353684
 },
 {
   "Name": "Quầy thuốc Long Hương",
   "address": "Phố Tăng, Xã Phú Châu",
   "Longtitude": 20.5432381,
   "Latitude": 106.3314475
 },
 {
   "Name": "Quầy thuốc Thanh Thu",
   "address": "Ngã 3, Phố Tăng, Xã Phong Châu",
   "Longtitude": 20.5443796,
   "Latitude": 106.3335493
 },
 {
   "Name": "Quầy thuốc Thanh Quyền",
   "address": "An Liêm",
   "Longtitude": 20.3454535,
   "Latitude": 106.0001018
 },
 {
   "Name": "Quầy thuốc Phạm Thị Luận",
   "address": "Thôn Trung, xã Đông Phương, huyện Đông Hưng",
   "Longtitude": 20.5872377,
   "Latitude": 106.3979346
 },
 {
   "Name": "Nhà thuốc Chemgreens",
   "address": "phố Phạm Huy Quang, TT Đông Hưng",
   "Longtitude": 20.5595884,
   "Latitude": 106.3541549
 },
 {
   "Name": "Quầy thuốc Trung Tâm Ds Trang",
   "address": "Tổ 3 TT Đông Hưng",
   "Longtitude": 20.5561898,
   "Latitude": 106.3544734
 },
 {
   "Name": "Quầy thuốc Thanh Yến",
   "address": "Phố Châu Giang, Xã Đông Á",
   "Longtitude": 20.5183354,
   "Latitude": 106.4119977
 },
 {
   "Name": "Nhà thuốc Anh Doanh",
   "address": "188, Khu 7, TT Diêm Điền",
   "Longtitude": 20.5641232,
   "Latitude": 106.5628266
 },
 {
   "Name": "Quầy thuốc Thanh Mai",
   "address": "Số 189 Khu 7 TT Diêm Điền",
   "Longtitude": 20.5641232,
   "Latitude": 106.5628266
 },
 {
   "Name": "QT Thi Mơ Ds Nguyễn Thị Mơ",
   "address": "Số 143 Khu 6 TT Diêm Điền",
   "Longtitude": 20.5607901,
   "Latitude": 106.5674696
 },
 {
   "Name": "Quầy thuốc Phúc Ngọc",
   "address": "Xã Thái Tân",
   "Longtitude": 20.4861198,
   "Latitude": 106.5333649
 },
 {
   "Name": "Quầy thuốc Du Hường Ds Lã Đức Du",
   "address": "Chợ Thượng Phúc, Xã Thụy Sơn",
   "Longtitude": 20.5584479,
   "Latitude": 106.5011406
 },
 {
   "Name": "Quầy thuốc Hà An",
   "address": "Chợ Giành, thôn An Định, Xã Thụy Văn",
   "Longtitude": 20.575386,
   "Latitude": 106.5228494
 },
 {
   "Name": "Quầy thuốc Sơn Giang Ds Làn",
   "address": "346, Khu 6, TT Diêm Điền",
   "Longtitude": 20.5604645,
   "Latitude": 106.5702387
 },
 {
   "Name": "Quầy thuốc Thanh Thủy",
   "address": "Khu 3, TT Diêm Điền",
   "Longtitude": 20.5549084,
   "Latitude": 106.566154
 },
 {
   "Name": "Quầy thuốc Văn Miến",
   "address": "Xóm 2, Xã Thụy Hưng",
   "Longtitude": 20.5893735,
   "Latitude": 106.4965434
 },
 {
   "Name": "Quầy thuốc Trinh Thủy",
   "address": "Chợ Gạch, Xã Thái Tân",
   "Longtitude": 20.4845644,
   "Latitude": 106.5400814
 },
 {
   "Name": "Quầy thuốc Ngọc Huyền",
   "address": "Thôn Văn Hàn Tây, Xã Thái Hưng",
   "Longtitude": 20.5119634,
   "Latitude": 106.5201084
 },
 {
   "Name": "Nhà thuốc doanh nghiệp số 1 Ds Thương",
   "address": "Số 35, Khu 4, TT Tiền Hải",
   "Longtitude": 20.406753,
   "Latitude": 106.5067107
 },
 {
   "Name": "Nhà thuốc Phương Nam DsTrần Thị Lanh",
   "address": "Xóm 1, Tây Sơn, TT Tiền Hải",
   "Longtitude": 20.402609,
   "Latitude": 106.5009617
 },
 {
   "Name": "Trung tâm dược Tiền Hải Ds Hoàng Thị Phương",
   "address": "Xóm 1, Tây Sơn, TT Tiền Hải",
   "Longtitude": 20.402609,
   "Latitude": 106.5009617
 },
 {
   "Name": "Quầy thuốc Trần Mơ",
   "address": "Xóm 2, Thôn Bắc Sơn",
   "Longtitude": 21.8437729,
   "Latitude": 106.2992912
 },
 {
   "Name": "Quầy thuốc Tuệ Tâm Ds Vũ Thị Mai",
   "address": "Chợ Tiểu Hoàng, TT Tiền Hải",
   "Longtitude": 20.4097452,
   "Latitude": 106.5081067
 },
 {
   "Name": "QT Vũ Thị Thu (Bà Hòa)",
   "address": "Cổng chợ Đông Xuyên, Xã Đông Xuyên",
   "Longtitude": 20.4426033,
   "Latitude": 106.5654007
 },
 {
   "Name": "Quầy thuốc Diệu Linh Ds Dệt",
   "address": "Thôn Đông, Xã Tây Giang",
   "Longtitude": 20.3917865,
   "Latitude": 106.5083256
 },
 {
   "Name": "Nhà thuốc Ngô Duy Thắng",
   "address": "Thôn Đông Xã Tây Giang",
   "Longtitude": 20.3917865,
   "Latitude": 106.5083256
 },
 {
   "Name": "Quầy thuốc Cẩm Bình",
   "address": "Thôn Ái Quốc, Xã Nam Thanh",
   "Longtitude": 20.3452205,
   "Latitude": 106.5497124
 },
 {
   "Name": "Công ty dược Ánh Dương Ds Trần Thị Thư",
   "address": "Kiot HTX Tiền Tiến, TT Tiền Hải",
   "Longtitude": 20.402609,
   "Latitude": 106.5009617
 },
 {
   "Name": "Quầy thuốc Ngọc Châu",
   "address": "Thôn Vĩnh Trà, Xã Nam Trung",
   "Longtitude": 20.3337761,
   "Latitude": 106.5407299
 },
 {
   "Name": "Nhà thuốc Hiểu Trang",
   "address": "Chợ Đác, Xã Vũ Lễ",
   "Longtitude": 20.434766,
   "Latitude": 106.3962475
 },
 {
   "Name": "Nhà thuốc Ngô Chiêm",
   "address": "34 Khu Cộng Hoà, TT Kiến Xương",
   "Longtitude": 20.4208256,
   "Latitude": 106.4170311
 },
 {
   "Name": "Quầy thuốc Trúc Nguyệt",
   "address": "Xã Lê Lợi",
   "Longtitude": 20.8316114,
   "Latitude": 105.9024322
 },
 {
   "Name": "Quầy thuốc Hoài Lan",
   "address": "Xóm 4, Xã Vũ Quý",
   "Longtitude": 20.4037136,
   "Latitude": 106.3846459
 },
 {
   "Name": "Quầy thuốc Hoa Chiêm",
   "address": "Xóm 4, Xã Vũ Quý",
   "Longtitude": 20.4037136,
   "Latitude": 106.3846459
 },
 {
   "Name": "Quầy thuốc Bùi Yến",
   "address": "Chợ Gốc, Xã Bình Thanh",
   "Longtitude": 10.8105831,
   "Latitude": 106.7091422
 },
 {
   "Name": "Quầy thuốc Lê Thị Thu Hiền",
   "address": "Thôn Hưng Đạo, xã Bình Định, huyện Kiến Xương",
   "Longtitude": 20.3948658,
   "Latitude": 106.4312255
 },
 {
   "Name": "Quầy thuốc Khuyến Vân",
   "address": "Xã Vũ Hội",
   "Longtitude": 20.4123196,
   "Latitude": 106.3552092
 },
 {
   "Name": "Đại Lý Thuốc Tân Dược Ds Trần Thị Nam.",
   "address": "chợ Đồn, xã Đồng Thanh, Vũ Thư",
   "Longtitude": 20.5079049,
   "Latitude": 106.2022072
 },
 {
   "Name": "Quầy thuốc Tân Dược Ds Nguyễn Thị Hồi",
   "address": "Khu 3 TT Vũ Thư",
   "Longtitude": 20.4359144,
   "Latitude": 106.2823717
 },
 {
   "Name": "Đại lý thuốc Tân Dược Ds Trần Thị Huyền",
   "address": "Chợ Búng, Thôn Mỹ Lộc 1, Xã Việt Hùng",
   "Longtitude": 20.4745418,
   "Latitude": 106.2277354
 },
 {
   "Name": "Nhà thuốc Thành An",
   "address": "Xóm 2, Khu Nguyễn Trãi, Xã Hòa Bình",
   "Longtitude": 20.4196424,
   "Latitude": 106.2786937
 },
 {
   "Name": "Quầy thuốc Tân Dược Ds Nguyễn Thị Hằng",
   "address": "Thôn Mỹ Lộc 2, Xã Việt Hùng",
   "Longtitude": 20.4416309,
   "Latitude": 106.1081103
 },
 {
   "Name": "Quầy thuốc Quyết Yến",
   "address": "Km 5 đường 223, Xã Tân Hòa",
   "Longtitude": 20.4656144,
   "Latitude": 106.3259157
 },
 {
   "Name": "Đại lý thuốc Tân Dược Ds Bùi Thị Lan",
   "address": "Phố La, Xã Minh Quang",
   "Longtitude": 21.0745356,
   "Latitude": 105.3189726
 },
 {
   "Name": "Quầy thuốc Huy Dung",
   "address": "Thôn Quý Sơn, Xã Song An",
   "Longtitude": 21.3723715,
   "Latitude": 106.4994889
 }
];