var datakhoidonvikhac = [
 {
   "STT": 1,
   "Name": "Chi cục dân số, kế hoạch hóa gia đình",
   "area": "Tỉnh",
   "address": "số 13 Lê Triệu Kiết, phường Mỹ Bình, Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3878345,
   "Latitude": 105.43692
 },
 {
   "STT": 2,
   "Name": "Chi cục an toàn vệ sinh thực phẩm",
   "area": "Tỉnh",
   "address": "108 Lê Minh Ngươn, P. Mỹ Long, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3845662,
   "Latitude": 105.4408835
 },
 {
   "STT": 3,
   "Name": "Trung tâm y tế dự phòng",
   "area": "Tỉnh",
   "address": "12B, Lê Lợi, P.Mỹ Bình, thành phố Long Xuyên",
   "Longtitude": 10.3871257,
   "Latitude": 105.4395485
 },
 {
   "STT": 4,
   "Name": "Trung tâm chăm sóc sức khỏe sinh sản",
   "area": "Tỉnh",
   "address": "79 Tôn Đức Thắng, phường Mỹ Bình, thành phố Long Xuyên",
   "Longtitude": 10.385608,
   "Latitude": 105.4365098
 },
 {
   "STT": 5,
   "Name": "Trung tâm truyền thông giáo dục sức khỏe",
   "area": "Tỉnh",
   "address": "10-11 Lê Lợi, phường Mỹ Bình, thành phố Long Xuyên",
   "Longtitude": 10.3906075,
   "Latitude": 105.4368881
 },
 {
   "STT": 6,
   "Name": "Trung tâm phòng chống HIV/AISD",
   "area": "Tỉnh",
   "address": "Số 20 Nguyễn Du, Phường Mỹ Bình, thành phố Long Xuyên",
   "Longtitude": 10.387413,
   "Latitude": 105.4403096
 },
 {
   "STT": 7,
   "Name": "Trung tâm kiểm nghiệm dược - Mỹ phẩm",
   "area": "Tỉnh",
   "address": "Số 20, Nguyễn Du - P. Mỹ Bình - thành phố Long Xuyên",
   "Longtitude": 10.387413,
   "Latitude": 105.4403096
 },
 {
   "STT": 8,
   "Name": "Trung tâm kiểm dịch y tế quốc tế",
   "area": "Tỉnh",
   "address": "Đường Phan Xích Long, P.Mỹ Thạnh, thành phố Long Xuyên",
   "Longtitude": 10.3355273,
   "Latitude": 105.4792378
 },
 {
   "STT": 9,
   "Name": "Trung tâm giám định y khoa",
   "area": "Tỉnh",
   "address": "10-11 Lê Lợi, phường Mỹ Bình, thành phố Long Xuyên",
   "Longtitude": 10.3906075,
   "Latitude": 105.4368881
 },
 {
   "STT": 10,
   "Name": "Trung tâm pháp y",
   "area": "Tỉnh",
   "address": "10-11 Lê Lợi, phường Mỹ Bình, thành phố Long Xuyên",
   "Longtitude": 10.3906075,
   "Latitude": 105.4368881
 }
];