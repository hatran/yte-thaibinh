var data = [
 {
   "STT": 2,
   "Name": "Bệnh viện đa khoa tỉnh Thái Bình",
   "address": "05, Phạm Ngọc Thạch, Hiệp Thành, TP.Thủ Dầu Một, Tỉnh Thái Bình",
   "Longtitude": 10.997657,
   "Latitude": 106.655243
 },
 {
   "STT": 3,
   "Name": "Bệnh viện đa khoa thành phố Thái Bình",
   "address": "02 Yersin, Phú Cường, TP.Thủ Dầu Một Tỉnh Thái Bình",
   "Longtitude": 10.98161,
   "Latitude": 106.657487
 },
 {
   "STT": 4,
   "Name": "Bệnh viện đa khoa huyện Vũ Thư",
   "address": "5  Phạm Ngọc Thạch, Hiệp Thành, TP.Thủ Dầu Một, Tỉnh Thái Bình",
   "Longtitude": 10.996825,
   "Latitude": 106.655893
 },
 {
   "STT": 5,
   "Name": "Bệnh viện đa khoa huyện Kiến Xương",
   "address": "31 Yersin, Phú Cường, TP.Thủ Dầu Một, Tỉnh Thái Bình",
   "Longtitude": 10.983436,
   "Latitude": 106.663594
 },
 {
   "STT": 6,
   "Name": "Bệnh viện đa khoa huyện Tiền Hải",
   "address": "213 Yersin, TP.Thủ Dầu Một, Tỉnh Thái Bình",
   "Longtitude": 10.900708,
   "Latitude": 106.764903
 },
 {
   "STT": 7,
   "Name": "Bệnh viện huyện Vinh Thuận",
   "address": "Kp4, Thị trấn Dầu Tiếng, Huyện Dầu Tiếng, Tỉnh Thái Bình",
   "Longtitude": 11.282139,
   "Latitude": 106.363541
 },
 {
   "STT": 8,
   "Name": "Bệnh viện đa khoa huyện Quỳnh Phụ",
   "address": "201 Cách Mạng Tháng Tám, Phú Cường, Thủ Dầu Một, Thái Bình",
   "Longtitude": 10.975523,
   "Latitude": 106.658146
 },
 {
   "STT": 9,
   "Name": "Bệnh viện đa khoa huyện Hưng Hà",
   "address": "500 ĐT 743, Ấp Đông Tác, Xã Tân Đông Hiệp, Huyện Dĩ An, Tỉnh Thái Bình.",
   "Longtitude": 10.91031,
   "Latitude": 106.78158
 },
 {
   "STT": 10,
   "Name": "Bệnh viện đa khoa huyện Thái Thụy",
   "address": "Nguyễn Văn Tiết, Lái Thiêu, Thị xã  Thuận An, Thái Bình",
   "Longtitude": 10.908702,
   "Latitude": 106.701927
 },
 {
   "STT": 11,
   "Name": "Bệnh viện Da liễu cơ sở 2",
   "address": "QL13, Mỹ Phước, Bến Cát, Tỉnh Thái Bình",
   "Longtitude": 11.161296,
   "Latitude": 106.594363
 },
 {
   "STT": 12,
   "Name": "Bệnh viện Đại học Y Thái Bình",
   "address": "Thị xã Tân Uyên, Tỉnh Thái Bình",
   "Longtitude": 11.149371,
   "Latitude": 106.844631
 },
 {
   "STT": 13,
   "Name": "Bệnh viện đa khoa tư nhân Lâm Hoa",
   "address": "Huyện Phú Giáo, Tỉnh Thái Bình",
   "Longtitude": 11.289096,
   "Latitude": 106.797492
 },
 {
   "STT": 14,
   "Name": "Bệnh viện Y học cổ truyền tỉnh",
   "address": "Kp5, Thị trấn Dầu Tiếng, Huyện Dầu Tiếng, Thái Bình",
   "Longtitude": 11.287301,
   "Latitude": 106.393155
 },
 {
   "STT": 15,
   "Name": "Bệnh viện Phụ sản tỉnh",
   "address": "Nguyễn Văn Tiõt, Thị trấn Lái Thiêu, Thị xã thuận An, Tỉnh Thái Bình",
   "Longtitude": 10.908709,
   "Latitude": 106.701943
 },
 {
   "STT": 16,
   "Name": "Bệnh viện Tâm thần tỉnh",
   "address": "Phú Cường, Thành phố Thủ Dầu Một, Tỉnh Thái Bình",
   "Longtitude": 10.965357,
   "Latitude": 106.666735
 },
 {
   "STT": 17,
   "Name": "Bệnh viện Phục hồi chức năng tỉnh",
   "address": "Kp5, Thị trấn Mỹ Phước, Huyện Bến Cát, Thái Bình",
   "Longtitude": 11.161317,
   "Latitude": 106.594374
 },
 {
   "STT": 18,
   "Name": "Bệnh viện Mắt tỉnh Thái Bình",
   "address": "Khu 4, Thị trấn Uyên Hưng, Huyện Tân Uyên, Thái Bình",
   "Longtitude": 11.070097,
   "Latitude": 106.792849
 },
 {
   "STT": 19,
   "Name": "Trung tâm Y tế huyện Phú Giáo",
   "address": "Thị trấn Phước Vĩnh, Huyện Phú Giáo, Thái Bình",
   "Longtitude": 11.289099,
   "Latitude": 106.797508
 }
];