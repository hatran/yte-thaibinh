var dataphongkham = [
 {
   "STT": 2,
   "Name": "Phòng khám Đa khoa khu vực Vĩnh Hòa",
   "address": "952; xã Vĩnh Hòa, THÁI BÌNH",
   "Longtitude": 10.8801661,
   "Latitude": 105.1804886
 },
 {
   "STT": 3,
   "Name": "Phòng khám Đa khoa khu vực Chợ Vàm",
   "address": "Thị trấn Chợ Vàm Phú Tân, THÁI BÌNH",
   "Longtitude": 10.702865,
   "Latitude": 105.3306814
 },
 {
   "STT": 4,
   "Name": "Phòng khám Đa khoa khu vực Thạnh Mỹ Tây",
   "address": "Ấp Đông Châu, xã Thạnh Mỹ Tây, huyện Châu Phú, THÁI BÌNH",
   "Longtitude": 10.5445292,
   "Latitude": 105.1473691
 },
 {
   "STT": 5,
   "Name": "Phòng khám Đa khoa khu vực Tịnh Biên",
   "address": "Hữu Nghĩa, Xuân Biên, Tịnh Biên, THÁI BÌNH",
   "Longtitude": 10.6038778,
   "Latitude": 104.9461162
 },
 {
   "STT": 6,
   "Name": "Phòng khám Đa khoa khu vực Chi Lăng",
   "address": "Thị trấn Chi Lăng, Tịnh Biên, THÁI BÌNH",
   "Longtitude": 10.5295468,
   "Latitude": 105.0177539
 },
 {
   "STT": 7,
   "Name": "Phòng khám Đa khoa khu vực Ba Chúc",
   "address": "Đường Ngô Tự Lợi, thị trấn Ba Chúc, huyện Tri Tôn, THÁI BÌNH",
   "Longtitude": 10.4943645,
   "Latitude": 104.9089042
 },
 {
   "STT": 8,
   "Name": "Phòng khám Đa khoa khu vực Long Giang",
   "address": "Ấp Long Thạnh 2, xã Long Giang, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.4683333,
   "Latitude": 105.4358333
 },
 {
   "STT": 9,
   "Name": "Phòng khám Đa khoa khu vực Mỹ Luông",
   "address": "Ấp Mỹ Hòa, thị trấn Mỹ Luông, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.4956501,
   "Latitude": 105.4937686
 },
 {
   "STT": 10,
   "Name": "Phòng khám Đa khoa khu vực Vĩnh Bình",
   "address": "Số 2991 tổ 8 ấp Vĩnh Lộc, xã Vĩnh Bình, huyện Châu Thành, THÁI BÌNH",
   "Longtitude": 10.4204614,
   "Latitude": 105.1843801
 },
 {
   "STT": 11,
   "Name": "Phòng khám Đa khoa khu vực Óc Eo",
   "address": "Đường Phan Thanh Giản, thị trấn Óc Eo, Thoại Sơn, THÁI BÌNH",
   "Longtitude": 10.2555353,
   "Latitude": 105.149753
 },
 {
   "STT": 12,
   "Name": "Nha khoa Sài Gòn- Chi nhánh Long Xuyên",
   "address": "317/2 Trần Hưng Đạo, Mỹ Long, Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3809505,
   "Latitude": 105.439126
 },
 {
   "STT": 13,
   "Name": "Phòng khám Bác sĩ Bùi Duy Tuế",
   "address": "487/4 Đông Thịnh B2, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3732108,
   "Latitude": 105.4497175
 },
 {
   "STT": 14,
   "Name": "Phòng khám Bác sĩ Bùi Thị Hồng Phê",
   "address": "550/14e Hà Hoàng Hổ, phường Mỹ Xuyên, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.374096,
   "Latitude": 105.41426
 },
 {
   "STT": 15,
   "Name": "Phòng khám Bác sĩ Bùi Văn Liêu",
   "address": "561 Võ Thị Sáu, phường Mỹ Xuyên, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3753208,
   "Latitude": 105.4358203
 },
 {
   "STT": 16,
   "Name": "Cơ Sở Chẩn Đoán Y Khoa - Phó Thị Ngọc Thụy",
   "address": "66 Tôn Đức Thắng thị xã Long Xuyên, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.4053403,
   "Latitude": 105.4459823
 },
 {
   "STT": 17,
   "Name": "Phòng khám Bác sĩ Hồ Thị Phương Linh",
   "address": "Tổ 2 Bình Khánh xã Mỹ Khánh, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3953269,
   "Latitude": 105.4208821
 },
 {
   "STT": 18,
   "Name": "Phòng khám Bác sĩ Hồ Thiên",
   "address": "67/3c Bùi Văn Danh, phường Mỹ Xuyên, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3825012,
   "Latitude": 105.4386611
 },
 {
   "STT": 19,
   "Name": "Phòng khám Bác sĩ Huỳnh Hoàng Huy",
   "address": "66 Nguyễn Hữu Cảnh, thị trấn Chợ Mới, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.5397244,
   "Latitude": 105.4021631
 },
 {
   "STT": 20,
   "Name": "Phòng Mạch Đông Nguyên Lương Y Huỳnh Kim Dung",
   "address": "607a/31 Trần Hưng Đạo, phường Bình Khánh, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3898804,
   "Latitude": 105.429874
 },
 {
   "STT": 21,
   "Name": "Phòng khám Bác sĩ Huỳnh Tân Xuân",
   "address": "Xã Mỹ Hội Đông, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.515886,
   "Latitude": 105.3541013
 },
 {
   "STT": 22,
   "Name": "Phòng khám Bác sĩ Huỳnh Thanh Tuyền",
   "address": "Xã Hòa Bình, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.3901353,
   "Latitude": 105.471249
 },
 {
   "STT": 23,
   "Name": "Khám bệnh Siêu Âm",
   "address": "6/10 Lê Triệu Kiết, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3882816,
   "Latitude": 105.4364884
 },
 {
   "STT": 24,
   "Name": "Phòng khám Bác sĩ Lâm Võ Hùng",
   "address": "125 Nguyễn Huệ B, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3810272,
   "Latitude": 105.4402981
 },
 {
   "STT": 25,
   "Name": "Phòng khám Bác sĩ Lê Bổn",
   "address": "7 Phan Đình Phùng, huyện Châu Đốc, THÁI BÌNH",
   "Longtitude": 10.7086965,
   "Latitude": 105.1203832
 },
 {
   "STT": 26,
   "Name": "Phòng khám Bác sĩ Lê Công Quận",
   "address": "34a1 Phạm Cự Lượng, phường Mỹ Phước, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3660166,
   "Latitude": 105.4471534
 },
 {
   "STT": 27,
   "Name": "Phòng khám Bác sĩ Lê Hồng Ngô",
   "address": "Vĩnh Chánh phường Vĩnh Mỹ, huyện Châu Đốc, THÁI BÌNH",
   "Longtitude": 10.714729,
   "Latitude": 105.1172304
 },
 {
   "STT": 28,
   "Name": "Phòng khám Bác sĩ Lê Hồng Sơn",
   "address": "Xã Kiến Thành, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.512995,
   "Latitude": 105.4126652
 },
 {
   "STT": 29,
   "Name": "Phòng khám Bác sĩ Lê Minh An",
   "address": "Thị trấn An Phú, huyện An Phú, THÁI BÌNH",
   "Longtitude": 10.7885968,
   "Latitude": 105.0937378
 },
 {
   "STT": 30,
   "Name": "Phòng khám Bác sĩ Lê Minh Hùng",
   "address": "144/5b Đông Thịnh A2 phường Mỹ Phước, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.375602,
   "Latitude": 105.44761
 },
 {
   "STT": 31,
   "Name": "Phòng khám Bác sĩ Lê Minh Tín - Võ Thanh Dũng",
   "address": "97 Nguyễn Huệ B, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.381373,
   "Latitude": 105.4406611
 },
 {
   "STT": 32,
   "Name": "Phòng khám Bác sĩ Lê Thành Tú",
   "address": "67/7 Bùi Văn Danh, phường Mỹ Xuyên, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3824793,
   "Latitude": 105.4386459
 },
 {
   "STT": 33,
   "Name": "Phòng khám Bác sĩ Lê Văn Tấn",
   "address": "213 Trần Hưng Đạo, phường Mỹ Bình, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.385486,
   "Latitude": 105.436058
 },
 {
   "STT": 34,
   "Name": "Phòng khám Bác sĩ Lương Hoàng Dũng",
   "address": "Thị trấn An Châu, huyện Châu Thành, THÁI BÌNH",
   "Longtitude": 10.4377694,
   "Latitude": 105.3892372
 },
 {
   "STT": 35,
   "Name": "Phòng khám Bác sĩ Lý Kim Bình",
   "address": "Thị trấn An Phú, huyện An Phú, THÁI BÌNH",
   "Longtitude": 10.7885968,
   "Latitude": 105.0937378
 },
 {
   "STT": 36,
   "Name": "Phòng khám Bác sĩ Nga",
   "address": "240/5 Lương Văn Cù, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3832264,
   "Latitude": 105.4397325
 },
 {
   "STT": 37,
   "Name": "Phòng khám Bác sĩ Nguyễn Bá Tùng",
   "address": "Xã Bình Hòa, huyện Châu Thành, THÁI BÌNH",
   "Longtitude": 10.4406356,
   "Latitude": 105.3306814
 },
 {
   "STT": 38,
   "Name": "Phòng khám Bác sĩ Nguyễn Hoàng Mai",
   "address": "48b2 Tôn Thất Thuyết, phường Bình Khánh, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3929327,
   "Latitude": 105.4261663
 },
 {
   "STT": 39,
   "Name": "Phòng khám Bác sĩ Nguyễn Ngọc Rạng",
   "address": "140/5 Nguyễn Thái Học, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3887404,
   "Latitude": 105.4375196
 },
 {
   "STT": 40,
   "Name": "Phòng khám Bác sĩ Nguyễn Thành Long",
   "address": "39b Lý Thái Tổ, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3771515,
   "Latitude": 105.4412556
 },
 {
   "STT": 41,
   "Name": "Phòng khám Bác sĩ Nguyễn Thị Bạch Huệ",
   "address": "Xã Hòa Bình, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.3901353,
   "Latitude": 105.471249
 },
 {
   "STT": 42,
   "Name": "Phòng khám Bác sĩ Nguyễn Thị Hoa",
   "address": "62/4a Lê Văn Nhung, phường Mỹ Bình, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3842825,
   "Latitude": 105.43658310000001
 },
 {
   "STT": 43,
   "Name": "Phòng khám Bác sĩ Nguyễn Thị Hoàng Vân",
   "address": "18 Nguyễn Du, phường Mỹ Bình, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3871186,
   "Latitude": 105.4404526
 },
 {
   "STT": 44,
   "Name": "Phòng khám Bác sĩ Nguyễn Tuấn Khanh",
   "address": "Lê Lợi, huyện Châu Đốc, THÁI BÌNH",
   "Longtitude": 10.7053792,
   "Latitude": 105.1297358
 },
 {
   "STT": 45,
   "Name": "Phòng khám Bác sĩ Nguyễn Văn Phúc",
   "address": "Ấp Long Quới 2, xã Long Điền B, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.5112548,
   "Latitude": 105.4478131
 },
 {
   "STT": 46,
   "Name": "Phòng khám Bác sĩ Nguyễn Xuân Mới",
   "address": "1048/53b Bình Khánh 1 phường Bình Đức, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3955883,
   "Latitude": 105.4155939
 },
 {
   "STT": 47,
   "Name": "Phòng khám Bác sĩ Phạm Cường Thịnh",
   "address": "Ấp Mỹ Hòa B, xã Mỹ Hội Đông, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.4915985,
   "Latitude": 105.354221
 },
 {
   "STT": 48,
   "Name": "Phòng khám Bác sĩ Phạm Ngọc Định",
   "address": "358 Lê Lợi, huyện Châu Đốc, THÁI BÌNH",
   "Longtitude": 10.702798,
   "Latitude": 105.1323956
 },
 {
   "STT": 49,
   "Name": "Phòng khám Bác sĩ Phạm Thị Các",
   "address": "11b Phan Văn Vàng, huyện Châu Đốc, THÁI BÌNH",
   "Longtitude": 10.7092439,
   "Latitude": 105.1185781
 },
 {
   "STT": 50,
   "Name": "Phòng khám Bác sĩ Phan Ngọc Thạchtt.An Phú",
   "address": "Thị trấn An Phú, huyện An Phú, THÁI BÌNH",
   "Longtitude": 10.7885968,
   "Latitude": 105.0937378
 },
 {
   "STT": 51,
   "Name": "Phòng khám Bác sĩ Phan Thành Phú",
   "address": "169/9 phường Bình Đức, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.4153842,
   "Latitude": 105.4009508
 },
 {
   "STT": 52,
   "Name": "Phòng khám Bác sĩ Phan Thị Thanh Nga",
   "address": "Xã Long Giang, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.4681887,
   "Latitude": 105.4360963
 },
 {
   "STT": 53,
   "Name": "Phòng khám Bác sĩ Phan Thị Trường Xuân",
   "address": "Lô 1d Lê Lai, phường Mỹ Bình, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.389867,
   "Latitude": 105.4367758
 },
 {
   "STT": 54,
   "Name": "Phòng khám Bác sĩ Sang",
   "address": "Xã Quốc Thái, huyện An Phú, THÁI BÌNH",
   "Longtitude": 10.8910813,
   "Latitude": 105.0732771
 },
 {
   "STT": 55,
   "Name": "Phòng khám Bác sĩ Tân",
   "address": "Xã Bình Hòa, huyện Châu Thành, THÁI BÌNH",
   "Longtitude": 10.4406356,
   "Latitude": 105.3306814
 },
 {
   "STT": 56,
   "Name": "Phòng khám Bác sĩ Thái Ngọc Biển",
   "address": "170 Ấp Thị 2, thị trấn Mỹ Luông, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.508065,
   "Latitude": 105.48979
 },
 {
   "STT": 57,
   "Name": "Phòng khám Bác sĩ Tô Ngọc Liêm",
   "address": "Xã Long Điền A, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.5325029,
   "Latitude": 105.4595306
 },
 {
   "STT": 58,
   "Name": "Phòng khám Bác sĩ Trần Hồng Phước",
   "address": "4 Phan Văn Vàng, phường Châu Phú B, huyện Châu Đốc, THÁI BÌNH",
   "Longtitude": 10.7092525,
   "Latitude": 105.1185718
 },
 {
   "STT": 59,
   "Name": "Phòng khám Bác sĩ Trần Minh Tứ",
   "address": "666 Trần Hưng Đạo, phường Mỹ Phước, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3685559,
   "Latitude": 105.4473385
 },
 {
   "STT": 60,
   "Name": "Phòng khám Bác sĩ Trần Phú Quới",
   "address": "Ấp Hòa Long 4, thị trấn An Châu, huyện Châu Thành, THÁI BÌNH",
   "Longtitude": 10.4359535,
   "Latitude": 105.384625
 },
 {
   "STT": 61,
   "Name": "Phòng khám Bác sĩ Trần Phước Dũng",
   "address": "Ấp Vĩnh Tây 1, xã Vĩnh Tế, huyện Châu Đốc, THÁI BÌNH",
   "Longtitude": 10.6732681,
   "Latitude": 105.0718158
 },
 {
   "STT": 62,
   "Name": "Phòng khám Bác sĩ Trần Phước Hồng",
   "address": "53/7 Ấp Vĩnh Tây 1, xã Vĩnh Tế, huyện Châu Đốc, THÁI BÌNH",
   "Longtitude": 10.6732681,
   "Latitude": 105.0718158
 },
 {
   "STT": 63,
   "Name": "Trung tâm Mắt THÁI BÌNH",
   "address": "12b Lê Lợi, phường Mỹ Bình, thành phố Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3871257,
   "Latitude": 105.4395485
 },
 {
   "STT": 64,
   "Name": "Phòng khám Bác sĩ Từ Quốc Tuấn",
   "address": "Thoại Ngọc Hầu, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.5503708,
   "Latitude": 105.4014323
 },
 {
   "STT": 65,
   "Name": "Phòng khám Bác sĩ Võ Minh Hiền",
   "address": "Tổ 26b Châu Long 6 phường Vĩnh Mỹ, huyện Châu Đốc, THÁI BÌNH",
   "Longtitude": 10.7028139,
   "Latitude": 105.1334585
 },
 {
   "STT": 66,
   "Name": "Phòng khám Bác sĩ Võ Minh Tánh",
   "address": "Ấp Kiến Quới 2, xã Kiến Thành, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.512995,
   "Latitude": 105.4126652
 },
 {
   "STT": 67,
   "Name": "Dịch Vụ Làm Răng Giả Thái Sơn",
   "address": "Lô 7H2 Hồ Quý Ly, phường Mỹ Quý, thành phố hường Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.36208,
   "Latitude": 105.4524494
 },
 {
   "STT": 68,
   "Name": "Dịch Vụ Y Tế Y sĩ Mai Minh Mẫn",
   "address": "Số 280/9D, tổ 12, Tây Khánh 8, phường Mỹ Hòa, thành phố hường Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3603108,
   "Latitude": 105.4126652
 },
 {
   "STT": 69,
   "Name": "Dịch Vụ Y Tế Đd. Trần Vũ Tâm",
   "address": "Số 45, tổ 2, ấp An Long, xã An Thạnh Trung, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.427419,
   "Latitude": 105.488828
 },
 {
   "STT": 70,
   "Name": "Dịch Vụ Y Tế Đd. Trịnh Nguyễn",
   "address": "Số 79/3, ấp Bờ Dâu, xã Thạnh Mỹ Tây, huyện Châu Phú, THÁI BÌNH",
   "Longtitude": 10.5308066,
   "Latitude": 105.1609838
 },
 {
   "STT": 71,
   "Name": "Dịch Vụ Y Tế Hs. Nguyễn Thị Thu Hà",
   "address": "Tổ 17, ấp Vĩnh Bình, xã Vĩnh Thạnh Trung, huyện Châu Phú, THÁI BÌNH",
   "Longtitude": 10.5779931,
   "Latitude": 105.2019295
 },
 {
   "STT": 72,
   "Name": "Dịch Vụ Y Tế Đd. Lê Minh Thà",
   "address": "Số 662, tổ 7, ấp Bình Quới 2, xã Bình Thạnh Đông, huyện Phú Tân, THÁI BÌNH",
   "Longtitude": 10.570495,
   "Latitude": 105.258745
 },
 {
   "STT": 73,
   "Name": "Dịch Vụ Y Tế Hs. Võ Thị Thùy Dung",
   "address": "Tổ 12, ấp Phú Thuận B, xã Phú Lâm, huyện Phú Tân, THÁI BÌNH",
   "Longtitude": 10.7368718,
   "Latitude": 105.3072646
 },
 {
   "STT": 74,
   "Name": "Dịch Vụ Kính Thuốc Thanh In",
   "address": "Số 139 Phan Đình Phùng, khóm Châu Quới 3, phường Châu Phú B, thành phố hường Châu Đốc, THÁI BÌNH",
   "Longtitude": 10.7073785,
   "Latitude": 105.1220281
 },
 {
   "STT": 75,
   "Name": "Dịch Vụ Kính Thuốc Italy",
   "address": "Số 228 Trần Hưng Đạo, phường Mỹ Bình, thành phố hường Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3847083,
   "Latitude": 105.4366003
 },
 {
   "STT": 76,
   "Name": "Phòng khám Bác sĩ Lương Phi Hùng",
   "address": "Ấp Mỹ Lương, thị trấn Phú Mỹ, huyện Phú Tân, THÁI BÌNH",
   "Longtitude": 10.5983748,
   "Latitude": 105.3555759
 },
 {
   "STT": 77,
   "Name": "Phòng khám Bác sĩ Châu Nhật Ly",
   "address": "Tổ 16, khóm Thới Hòa, thị trấn Nhà Bàng, huyện Tịnh Biên, THÁI BÌNH",
   "Longtitude": 10.6231047,
   "Latitude": 105.0060673
 },
 {
   "STT": 78,
   "Name": "Phòng Chẩn Trị Y học cổ truyền Hội Đông Y Thị Trấn Tịnh Biên",
   "address": "Khóm Xuân Biên, thị trấn Tịnh Biên, huyện Tịnh Biên, THÁI BÌNH",
   "Longtitude": 10.6186839,
   "Latitude": 104.9495987
 },
 {
   "STT": 79,
   "Name": "Phòng khám Bác sĩ Nguyễn Thanh Cao",
   "address": "Số 495, đường Châu Long 6, phường Vĩnh Mỹ, thành phố hường Châu Đốc, THÁI BÌNH",
   "Longtitude": 10.7028139,
   "Latitude": 105.1334585
 },
 {
   "STT": 80,
   "Name": "Phòng khám Bác sĩ Lê Thành Thanh Vũ",
   "address": "Số 24/1 Lê Lợi, phường Châu Phú B, thành phố hường Châu Đốc, THÁI BÌNH",
   "Longtitude": 10.7063487,
   "Latitude": 105.1283497
 },
 {
   "STT": 81,
   "Name": "Phòng khám Bác sĩ Nguyễn Hồng Nam",
   "address": "Số 198 Trương Định, phường Châu Phú B, thành phố hường Châu Đốc, THÁI BÌNH",
   "Longtitude": 10.7013779,
   "Latitude": 105.1261401
 },
 {
   "STT": 82,
   "Name": "Phòng khám Bác sĩ Nguyễn Hữu Tuấn",
   "address": "Số 176 Trương Định, phường Châu Phú B, thành phố hường Châu Đốc, THÁI BÌNH",
   "Longtitude": 10.7022983,
   "Latitude": 105.126848
 },
 {
   "STT": 83,
   "Name": "Dịch Vụ Làm Răng Giả Hoàng Mai",
   "address": "Ấp An Hưng, thị trấn An Phú, huyện An Phú, THÁI BÌNH",
   "Longtitude": 10.7885968,
   "Latitude": 105.0937378
 },
 {
   "STT": 84,
   "Name": "Dịch Vụ Làm Răng Giả Trương Hưng",
   "address": "Số 7/47B, Thới Hòa, phường Mỹ Thạnh, thành phố hường Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3334683,
   "Latitude": 105.4800424
 },
 {
   "STT": 85,
   "Name": "Dịch Vụ Làm Răng Giả Dương Hiền",
   "address": "Số 11/42, Thới Hòa, phường Mỹ Thạnh, thành phố hường Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3334683,
   "Latitude": 105.4800424
 },
 {
   "STT": 86,
   "Name": "Dịch Vụ Y Tế Đd. Mai Thị Bé Thơ",
   "address": "Chợ Vĩnh Thành, ấp Tân Thành, xã Vĩnh Thành, huyện Châu Thành, THÁI BÌNH",
   "Longtitude": 10.3582121,
   "Latitude": 105.3190371
 },
 {
   "STT": 87,
   "Name": "Dịch Vụ Y Tế Tuấn Kiệt",
   "address": "Tổ 10, ấp Vĩnh Lộc, xã Vĩnh Bình, huyện Châu Thành, THÁI BÌNH",
   "Longtitude": 10.4470354,
   "Latitude": 105.172579
 },
 {
   "STT": 88,
   "Name": "Dịch Vụ Y Tế Hs. Hồ Thị Bích Lệ",
   "address": "Ấp Mỹ Thới, xã Định Mỹ, huyện Thoại Sơn, THÁI BÌNH",
   "Longtitude": 10.3348473,
   "Latitude": 105.2914238
 },
 {
   "STT": 89,
   "Name": "Dịch Vụ Y Tế Y sĩ Bùi Văn Quân",
   "address": "Ấp An Thịnh, thị trấn An Phú, huyện An Phú, THÁI BÌNH",
   "Longtitude": 10.7885968,
   "Latitude": 105.0937378
 },
 {
   "STT": 90,
   "Name": "Dịch Vụ Y Tế Y sĩ Nguyễn Thành Dũng",
   "address": "Số 224, tổ 6, ấp Long Hòa, xã Long Giang, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.4681887,
   "Latitude": 105.4360963
 },
 {
   "STT": 91,
   "Name": "Phòng Xét Nghiệm Cn. Trần Thị Mai Lan",
   "address": "Số 101A, Tỉnh lộ 942, ấp Long Hòa, thị trấn Chợ Mới, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.5519586,
   "Latitude": 105.4048437
 },
 {
   "STT": 92,
   "Name": "Phòng khám Bác sĩ Nguyễn Chánh Thành",
   "address": "Ấp Long Phú 2, xã Long Điền B, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.5104005,
   "Latitude": 105.4320465
 },
 {
   "STT": 93,
   "Name": "Phòng khám Bác sĩ Mai Thanh Bình",
   "address": "Ấp Hòa Trung, xã Kiến An, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.56379,
   "Latitude": 105.3749518
 },
 {
   "STT": 94,
   "Name": "Phòng Chẩn Trị Y học cổ truyền Ly. Lê Hữu Phương",
   "address": "Ấp Long Phú 1, xã Long Điền B, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.5111111,
   "Latitude": 105.4477778
 },
 {
   "STT": 95,
   "Name": "Phòng khám Bác sĩ Nhan Văn Bảy",
   "address": "Số 298, tổ 10, ấp Nam Huề, xã Bình Thành, huyện Thoại Sơn, THÁI BÌNH",
   "Longtitude": 10.2179592,
   "Latitude": 105.2019295
 },
 {
   "STT": 96,
   "Name": "Phòng Chẩn Trị Y học cổ truyền Ly. Hứa Mỹ Hương",
   "address": "Đường Nguyễn Văn Trỗi, tổ 14, ấp Đông Sơn 2, thị trấn Núi Sập, huyện Thoại Sơn, THÁI BÌNH",
   "Longtitude": 10.2653587,
   "Latitude": 105.2682671
 },
 {
   "STT": 97,
   "Name": "Phòng khám Bác sĩ Nguyễn Thị Xa",
   "address": "Tổ 10, ấp Vĩnh Lộc, xã Vĩnh Bình, huyện Châu Thành, THÁI BÌNH",
   "Longtitude": 10.4470354,
   "Latitude": 105.172579
 },
 {
   "STT": 98,
   "Name": "Phòng khám Bác sĩ Lâm Ngọc Trang",
   "address": "Số 86 Bùi Văn Danh, phường Mỹ Xuyên, thành phố hường Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3799724,
   "Latitude": 105.4355908
 },
 {
   "STT": 99,
   "Name": "Phòng khám Bác sĩ Huỳnh Thị Ngọc Bích",
   "address": "Số 161/3 Nguyễn Thái Học, phường Mỹ Bình, thành phố hường Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3889939,
   "Latitude": 105.4376283
 },
 {
   "STT": 100,
   "Name": "Phòng khám Bác sĩ Trương Thị Mỹ Tiến",
   "address": "Số 9C1 Cao Thắng, phường Bình Khánh, thành phố hường Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3922328,
   "Latitude": 105.422903
 },
 {
   "STT": 101,
   "Name": "Phòng khám Bác sĩ Nguyễn Tuấn Đoài",
   "address": "Số 8K1 Hà Huy Tập, phường Mỹ Phước, thành phố hường Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3645242,
   "Latitude": 105.4432568
 },
 {
   "STT": 102,
   "Name": "Phòng khám Bác sĩ Đặng Đức Trí",
   "address": "Số 116, tổ 6, đường Ven Bãi, khóm Châu Long 6, phường Vĩnh Mỹ, thành phố hường Châu Đốc, THÁI BÌNH",
   "Longtitude": 10.7028139,
   "Latitude": 105.1334585
 },
 {
   "STT": 103,
   "Name": "Phòng Chẩn Trị Y học cổ truyền Bảo Dân",
   "address": "Tổ 14, ấp Bình Minh, xã Bình Mỹ, huyện Châu Phú, THÁI BÌNH",
   "Longtitude": 10.5250878,
   "Latitude": 105.2779983
 },
 {
   "STT": 104,
   "Name": "Phòng khám Bác sĩ Võ Kim Nga",
   "address": "Số 239 Tôn Đức Thắng, khóm Long Thị D, phường Long Thạnh, thị xã Tân Châu, THÁI BÌNH",
   "Longtitude": 10.799139,
   "Latitude": 105.2450058
 },
 {
   "STT": 105,
   "Name": "Dịch Vụ Y Tế Y sĩ Trần Văn Nghĩa",
   "address": "Số 76/34, khóm Hưng Thạnh, phường Mỹ Thạnh, thành phố hường Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3247736,
   "Latitude": 105.471249
 },
 {
   "STT": 106,
   "Name": "Dịch Vụ Y Tế Đd. Nguyễn Ngọc Thi",
   "address": "Số 306/1A, tổ 26, khóm Tây Huề 3, phường Mỹ Hòa, thành phố hường Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3603108,
   "Latitude": 105.4126652
 },
 {
   "STT": 107,
   "Name": "Dịch Vụ Y Tế Đd. Trần Văn An",
   "address": "Ấp Vĩnh Thành, xã Vĩnh Trường, huyện An Phú, THÁI BÌNH",
   "Longtitude": 10.7790702,
   "Latitude": 105.1200482
 },
 {
   "STT": 108,
   "Name": "Dịch Vụ Kính Thuốc Nhân Đạo",
   "address": "Số 2EO Hàm Nghi, phường Bình Khánh, thành phố hường Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3931428,
   "Latitude": 105.4218372
 },
 {
   "STT": 109,
   "Name": "Dịch Vụ Y Tế Y sĩ Trần Thanh Bình",
   "address": "Khóm An Bình, thị trấn Ba Chúc, huyện Tri Tôn, THÁI BÌNH",
   "Longtitude": 10.489575,
   "Latitude": 104.903946
 },
 {
   "STT": 110,
   "Name": "Dịch Vụ Y Tế Y sĩ Trần Văn Liêm",
   "address": "Số 19 Trần Thanh Lạc, khóm An Hòa B, thị trấn Ba Chúc, huyện Tri Tôn, THÁI BÌNH",
   "Longtitude": 10.4942823,
   "Latitude": 104.9095979
 },
 {
   "STT": 111,
   "Name": "Phòng khám Bác sĩ Trần Nguyễn Hòa Hưng",
   "address": "Số 393, ấp Nhơn Lợi, xã Nhơn Mỹ, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.4589745,
   "Latitude": 105.41181
 },
 {
   "STT": 112,
   "Name": "Phòng khám Bác sĩ Lê Thị Mãi",
   "address": "Tổ 24, ấp An Thuận, xã Hòa Bình, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.3935508,
   "Latitude": 105.4566962
 },
 {
   "STT": 113,
   "Name": "Phòng khám Bác sĩ Trần Quốc Đạt",
   "address": "Số 737, tổ 27, ấp Bình Phú, xã Hòa An, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.4917136,
   "Latitude": 105.1785307
 },
 {
   "STT": 114,
   "Name": "Dịch Vụ Làm Răng Giả Trương Quang Nghĩa",
   "address": "Ấp Mỹ Hội, xã Mỹ Hội Đông, huyện Chợ Mới, THÁI BÌNH",
   "Longtitude": 10.4904783,
   "Latitude": 105.356108
 },
 {
   "STT": 115,
   "Name": "Dịch Vụ Làm Răng Giả Sáu Thành",
   "address": "Tổ 12, ấp Cần Thạnh, xã Cần Đăng, huyện Châu Thành, THÁI BÌNH",
   "Longtitude": 10.4506751,
   "Latitude": 105.2981186
 },
 {
   "STT": 116,
   "Name": "Phòng khám Nha Phượng Hằng",
   "address": "Số 158, tổ 7, ấp Phú Lợi, xã Phú Lâm, huyện Phú Tân, THÁI BÌNH",
   "Longtitude": 10.7282651,
   "Latitude": 105.2604409
 },
 {
   "STT": 117,
   "Name": "Phòng Chẩn Đoán Y Khoa Hoàn Hảo 2",
   "address": "Số 80A Tôn Đức Thắng, ấp Thượng 2, thị trấn Phú Mỹ, huyện Phú Tân, THÁI BÌNH",
   "Longtitude": 10.5900512,
   "Latitude": 105.3453184
 },
 {
   "STT": 118,
   "Name": "Phòng khám Chẩn Đoán Hình Ảnh - Bác sĩ Nguyễn Trần Nhã Khoa",
   "address": "Số 7/2 Lê Triệu Kiết, phường Mỹ Bình, thành phố hường Long Xuyên, THÁI BÌNH",
   "Longtitude": 10.3858158,
   "Latitude": 105.4374252
 },
 {
   "STT": 119,
   "Name": "Phòng khám Đa Khoa Khu Vực Đồng Ky",
   "address": "Ấp Đồng Ky, xã Quốc Thái, huyện An Phú, THÁI BÌNH",
   "Longtitude": 10.8933333,
   "Latitude": 105.0805556
 }
];